package ar.edu.itba.it.paw.repositories;

import ar.edu.itba.it.paw.domain.Dish;

public interface DishRepo {
	
	public Dish getDish(int dishid);
	
	public void add(Dish dish);
}
