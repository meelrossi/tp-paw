package ar.edu.itba.it.paw.web.pages.restaurant;

import java.util.Calendar;
import java.util.List;

import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import ar.edu.itba.it.paw.domain.ClosingTerm;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.repositories.ClosingTermRepo;
import ar.edu.itba.it.paw.repositories.RestaurantRepo;
import ar.edu.itba.it.paw.web.FoodNowSession;
import ar.edu.itba.it.paw.web.pages.BasePage;
import ar.edu.itba.it.paw.web.pages.login.FoodNowFeedbackPanel;

public class ClosingTermPage extends BasePage{

	private static final long serialVersionUID = 1L;
	
	@SpringBean
	private ClosingTermRepo closingTermRepo;
	
	@SpringBean
	private RestaurantRepo restaurantRepo;
	
	private transient String motive;
	private transient Calendar startDate;
	private transient Calendar endDate;
	
	public ClosingTermPage(final Restaurant restaurant) {
		add(new FoodNowFeedbackPanel("feedback"));
		Form<ClosingTermPage> closingTermForm = new Form<ClosingTermPage>("closingTermForm",
				new CompoundPropertyModel<ClosingTermPage>(this)) {
					private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit() {
				FoodNowSession s = FoodNowSession.get();
				List<String> managers = restaurant.getAllManagerMails();
				if(managers.contains(s.getEmail())) {
					ClosingTerm closingTerm = new ClosingTerm(startDate, endDate, motive, restaurant);
					closingTermRepo.add(closingTerm);
					restaurant.addClosingTerm(closingTerm);
					setResponsePage(new RestaurantPage(restaurant.getId()));
				}
			}
		};
		closingTermForm.add(new TextArea<String>("motive").add(
				new MaximumLengthValidator(ClosingTerm.MOTIVE_MAX_SIZE))
				.setRequired(true));
		closingTermForm.add(new DateTextField("startDate").setRequired(true));
		closingTermForm.add(new DateTextField("endDate").setRequired(true));
		closingTermForm.add(new Button("submit", new ResourceModel("submit")));
		add(closingTermForm);
	}

}
