package ar.edu.itba.it.paw.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Neighborhood extends PersistentEntity{
	
	private static final long serialVersionUID = 1L;

	public final static int NEIGHBORHOOD_MAX_SIZE = 30;
	
	@Column(nullable = false, length = NEIGHBORHOOD_MAX_SIZE)
	private String name;
	
	public Neighborhood() {
		
	}
	
	public Neighborhood(String name){
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String aName) {
		if (aName != null && aName.length() != 0 && aName.length() <= NEIGHBORHOOD_MAX_SIZE) {
			this.name = aName;
		}
	}
	
	public boolean equals(Neighborhood nb) {
		if(nb == null) {
			return false;
		}
		return name.equals(nb.getName());
	}
}
