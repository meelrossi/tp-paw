package ar.edu.itba.it.paw.web.pages.restaurant;

import java.io.Serializable;

import ar.edu.itba.it.paw.domain.DeliveryNeighborhood;
import ar.edu.itba.it.paw.domain.Neighborhood;

public class DeliveryNeighborhoodWrapper implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private DeliveryNeighborhood name;
	private Boolean selected = Boolean.FALSE;
	
	public DeliveryNeighborhoodWrapper(Neighborhood n) {
		this.name = new DeliveryNeighborhood(n);
	}
	
	public Boolean getSelected()
    {
        return selected;
    }
 
    public void setSelected(Boolean selected)
    {
        this.selected = selected;
    }
 
    public String getName()
    {
        return name.getNeighborhood().getName();
    }
    
    public DeliveryNeighborhood getDeliveryNeighborhood() {
    	return name;
    }
 
    public void setName(DeliveryNeighborhood wrapped)
    {
        this.name = wrapped;
    }
     
    public String toString()
    {
        return name.getNeighborhood().getName() + ": " + selected;
    }

}
