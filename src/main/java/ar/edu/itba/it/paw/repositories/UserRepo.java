package ar.edu.itba.it.paw.repositories;

import java.util.List;

import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.domain.User;

public interface UserRepo {
	public User get(String email);

	public void add(User user);

	public User get(Integer id);

	public List<User> getAllUsers();
	
	public List<String> getAllUserNames();
	
	public List<String> getAllUsersMails();
	
	public List<String> getAllManagersMails();
	
	public List<String> getAllManagersMails(Restaurant restaurant);
	
	public void delete(User user);

}
