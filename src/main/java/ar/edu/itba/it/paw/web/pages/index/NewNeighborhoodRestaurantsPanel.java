package ar.edu.itba.it.paw.web.pages.index;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.domain.EntityResolver;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.domain.User;
import ar.edu.itba.it.paw.repositories.RestaurantRepo;
import ar.edu.itba.it.paw.repositories.UserRepo;
import ar.edu.itba.it.paw.web.FoodNowSession;
import ar.edu.itba.it.paw.web.pages.restaurant.RestaurantPage;

public class NewNeighborhoodRestaurantsPanel extends Panel {
	private static final long serialVersionUID = 1L;

	@SpringBean
	private EntityResolver entityResolver;

	@SpringBean
	private RestaurantRepo restaurantRepo;
	
	@SpringBean
	private UserRepo userRepo;

	public NewNeighborhoodRestaurantsPanel(String id) {
		super(id);
		initialize();
	}

	private void initialize() {
		add(new RefreshingView<Restaurant>("newNeighborhoodRestaurant") {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<Restaurant>> getItemModels() {
				List<IModel<Restaurant>> result = new ArrayList<IModel<Restaurant>>();
				User user = userRepo.get(FoodNowSession.get().getEmail());
				for (Restaurant a : restaurantRepo.newRestaurants(user)) {
					final int id = a.getId();
					result.add(new LoadableDetachableModel<Restaurant>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected Restaurant load() {
							return entityResolver.fetch(Restaurant.class, id);
						}
					});
				}
				return result.iterator();
			}

			@SuppressWarnings({ "rawtypes" })
			@Override
			protected void populateItem(final Item<Restaurant> item) {
				item.add(new Link<Void>("restaurantLink") {
					private static final long serialVersionUID = 1L;

					@Override
					public void onClick() {
						setResponsePage(new RestaurantPage(item.getModelObject().getId()));
					}
					
				});
				item.add(new Label("name", item.getModelObject().getName()));
				item.add(new Label("address", item.getModelObject().getAddress().toString()));
				item.add(new NumberTextField("rating")
						.add(new SimpleAttributeModifier("value", String
								.valueOf(item.getModelObject().getRating()))));
			}
		});
	}
}
