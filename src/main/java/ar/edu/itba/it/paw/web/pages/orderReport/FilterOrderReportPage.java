package ar.edu.itba.it.paw.web.pages.orderReport;

import java.util.Calendar;

import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.ResourceModel;

import ar.edu.itba.it.paw.web.pages.BasePage;
import ar.edu.itba.it.paw.web.pages.login.FoodNowFeedbackPanel;

public class FilterOrderReportPage extends BasePage {
	
	private transient Calendar start;
	private transient Calendar end;
	
	public FilterOrderReportPage() {
		
		add(new FoodNowFeedbackPanel("feedback"));
		
		Form<FilterOrderReportPage> filterDateForm = new Form<FilterOrderReportPage>("filterDateForm",
				new CompoundPropertyModel<FilterOrderReportPage>(this)) {

			@Override
			protected void onSubmit() {
				setResponsePage(new OrderReportPage(start, end));
			}
		};
		add(filterDateForm);
		
		filterDateForm.add(new DateTextField("start").setRequired(true));
		filterDateForm.add(new DateTextField("end").setRequired(true));
		
		filterDateForm.add(new Button("regsubmit", new ResourceModel("submit")));

	}

}
