package ar.edu.itba.it.paw.web.pages.restaurant;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.domain.EntityResolver;
import ar.edu.itba.it.paw.domain.Rating;
import ar.edu.itba.it.paw.domain.Restaurant;

@SuppressWarnings("deprecation")
public class RatingPanel extends Panel{

	private static final long serialVersionUID = 1L;
	
	@SpringBean
	private EntityResolver entityResolver;
	
	public RatingPanel(String id, IModel<Restaurant> model) {
		super(id);
		initialize(model);
		// TODO Auto-generated constructor stub
	}
	
	private void initialize(final IModel<Restaurant> model) {
		add(new RefreshingView<Rating>("rating") {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<Rating>> getItemModels() {
				List<IModel<Rating>> result = new ArrayList<IModel<Rating>>();
				for (Rating c : model.getObject().getAllRatings()) {
					final int id = c.getId();
					result.add(new LoadableDetachableModel<Rating>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected Rating load() {
							return entityResolver.fetch(Rating.class, id);
						}
					});
				}
				return result.iterator();
			}

			@SuppressWarnings("rawtypes")
			@Override
			protected void populateItem(Item<Rating> item) {
				Rating rat = item.getModelObject();
				item.add(new Label("userName", rat.getUser().getName() + " " + rat.getUser().getLastname()));
				item.add(new Label("description", rat.getDescription()));
				item.add(new NumberTextField("ratingValue")
						.add(new SimpleAttributeModifier("value", String
								.valueOf(item.getModelObject().getRating()))));
			}
		});
	}

}
