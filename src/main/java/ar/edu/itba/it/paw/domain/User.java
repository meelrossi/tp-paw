package ar.edu.itba.it.paw.domain;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.servlet.http.HttpSession;

@Entity
@Table(name = "fn_user")
public class User extends PersistentEntity {

	private static final long serialVersionUID = 1L;
	
	public final static int FIRST_NAME_MAX_SIZE = 20;
	public final static int LAST_NAME_MAX_SIZE = 20;
	public final static int EMAIL_MAX_SIZE = 50;
	public final static int PASSWORD_MAX_SIZE = 20;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Calendar lastLogin;

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar previousLogin;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Calendar lastPasswordChange;

	@Column(nullable = false, length = FIRST_NAME_MAX_SIZE)
	private String name;

	@Column(nullable = false, length = LAST_NAME_MAX_SIZE)
	private String lastname;

	@OneToOne(targetEntity = Address.class, cascade = CascadeType.REMOVE)
	private Address address;

	@Column(nullable = false, length = EMAIL_MAX_SIZE, unique = true)
	private String email;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Calendar birthDate;

	@Column(nullable = false)
	private boolean isBanned = false;

	@Transient
	private HttpSession session;

	@Column(nullable = false, length = PASSWORD_MAX_SIZE)
	private String password;

	@Column(nullable = false)
	private Integer userLevel;

	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	List<Restaurant> restaurants;

	@OneToMany(cascade = CascadeType.ALL)
	List<FNOrder> orders;

	public User() {

	}

	public User(String name, String lastname, Address address, String email, String password, Calendar birthDate,
			Integer userLevel) {
		super();
		setName(name);
		setLastname(lastname);
		setAddress(address);
		setEmail(email);
		setPassword(password);
		setBirthDate(birthDate);
		setUserLevel(userLevel);
		Calendar firstDate = Calendar.getInstance();
		firstDate.set(1980, Calendar.JANUARY , 1);
		setLastLogin(firstDate);
		restaurants = new LinkedList<Restaurant>();
		this.lastLogin = Calendar.getInstance();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null || name.length() == 0 || name.length() > FIRST_NAME_MAX_SIZE) {
			throw new IllegalStateException();
		}
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		if (lastname == null || lastname.length() == 0 || lastname.length() > LAST_NAME_MAX_SIZE) {
			throw new IllegalStateException();
		}
		this.lastname = lastname;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		if (address == null) {
			throw new IllegalStateException();
		}
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if (email == null || email.length() == 0 || email.length() > EMAIL_MAX_SIZE
				|| !email.matches("[a-z0-9._%+-]+@[a-z0-9]+(\\.[a-z0-9]+)*(\\.[a-z]{2,})")) {
			throw new IllegalStateException();
		}
		this.email = email;
	}

	public Calendar getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Calendar birthDate) {
		this.birthDate = birthDate;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		if (session == null) {
			throw new IllegalStateException();
		}
		this.session = session;
	}

	public int getUserLevel() {
		return this.userLevel;
	}

	public void setUserLevel(Integer userLevel) {
		if (userLevel < 0 || userLevel > 2) {
			throw new IllegalStateException();
		}
		this.userLevel = userLevel;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		if (password == null || password.length() == 0 || password.length() > PASSWORD_MAX_SIZE) {
			throw new IllegalStateException();
		}
		this.password = password;
		this.lastPasswordChange = Calendar.getInstance();
		System.out.println(lastPasswordChange);
	}

	public void addRestaurant(Restaurant res) {
		if (res == null) {
			throw new IllegalStateException();
		}
		if (userLevel > 1) {
			throw new IllegalStateException();
		}
		if (restaurants == null) {
			restaurants = new LinkedList<Restaurant>();
		}
		restaurants.add(res);
	}

	public List<Restaurant> getRestaurants() {
		return this.restaurants;
	}

	public List<FNOrder> getOrders() {
		return orders;
	}

	public void addOrder(FNOrder order) {
		if (order == null) {
			throw new IllegalStateException();
		}
		if (orders == null) {
			orders = new LinkedList<FNOrder>();
		}
		orders.add(order);
	}

	public boolean checkPassword(String pass) {
		return this.password.equals(pass);
	}

	public boolean isBanned() {
		return this.isBanned;
	}

	public void setBanned() {
		this.isBanned = !this.isBanned;
	}
	
	public Calendar getLastPasswordChange() {
		return this.lastPasswordChange;
	}
	
	public void setLastLogin(Calendar date) {
		this.lastLogin = date;
	}
	
	public Calendar getLastLogin() {
		return this.lastLogin;
	}	
	
	public Calendar getPreviousLogin() {
		return previousLogin;
	}

	public void setPreviousLogin(Calendar previousLogin) {
		this.previousLogin = previousLogin;
	}
	
	public boolean equals(User user) {
		return user.getEmail().equals(email);
	}
}
