package ar.edu.itba.it.paw.web.application;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.wicket.Component;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.authorization.IAuthorizationStrategy;
import org.apache.wicket.request.component.IRequestableComponent;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.domain.Action;
import ar.edu.itba.it.paw.repositories.ActionRepo;
import ar.edu.itba.it.paw.web.FoodNowSession;
import ar.edu.itba.it.paw.web.pages.login.LoginPage;

public class FoodNowAuthorizationStrategy implements IAuthorizationStrategy {

	private static List<String> loginRequiredPages = Arrays
			.asList("ar.edu.itba.it.paw.web.pages.update.UpdatePage", "ar.edu.itba.it.paw.web.pages.report.dsManagerReportPage");

	public <T extends IRequestableComponent> boolean isInstantiationAuthorized(
			Class<T> componentClass) {

		if (loginRequiredPages.contains(componentClass.getName())) {
			if(!FoodNowSession.get().isSignedIn()) {
				throw new RestartResponseAtInterceptPageException(LoginPage.class);
			}
		}
			
		return true;
	}

	public boolean isActionAuthorized(Component component, org.apache.wicket.authorization.Action action) {
		// TODO Auto-generated method stub
		return true;
	}
	
}
