package ar.edu.itba.it.paw.web.application;

import org.apache.wicket.ConverterLocator;
import org.apache.wicket.IConverterLocator;
import org.apache.wicket.Page;
import org.apache.wicket.Session;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ar.edu.itba.it.paw.web.FoodNowSession;
import ar.edu.itba.it.paw.web.HibernateRequestCycleListener;
import ar.edu.itba.it.paw.web.pages.addmanager.AddManagerPage;
import ar.edu.itba.it.paw.web.pages.banuser.BanUserPage;
import ar.edu.itba.it.paw.web.pages.index.IndexPage;
import ar.edu.itba.it.paw.web.pages.login.ChangePasswordPage;
import ar.edu.itba.it.paw.web.pages.login.LoginPage;
import ar.edu.itba.it.paw.web.pages.orderReport.FilterOrderReportPage;
import ar.edu.itba.it.paw.web.pages.orderReport.OrderReportPage;
import ar.edu.itba.it.paw.web.pages.orders.OrderPage;
import ar.edu.itba.it.paw.web.pages.report.ManagerReportPage;
import ar.edu.itba.it.paw.web.pages.restaurant.AssignManagerPage;
import ar.edu.itba.it.paw.web.pages.restaurant.RatingPage;
import ar.edu.itba.it.paw.web.pages.restaurant.RestaurantListPage;
import ar.edu.itba.it.paw.web.pages.restaurant.RestaurantPage;
import ar.edu.itba.it.paw.web.pages.restaurant.addDish.AddDishPage;
import ar.edu.itba.it.paw.web.pages.restaurant.addRestaurant.RestaurantAddPage;
import ar.edu.itba.it.paw.web.pages.update.UpdatePage;
import ar.edu.itba.it.paw.web.pages.userReport.UsersReportPage;

@Component
public class FoodNowApplication extends WebApplication {

	private SessionFactory sessionFactory;

	@Autowired
	public FoodNowApplication(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Class<? extends Page> getHomePage() {
		return IndexPage.class;
	}

	@Override
	protected void init() {
		super.init();
		getComponentInstantiationListeners().add(new SpringComponentInjector(this));
		getRequestCycleListeners().add(new HibernateRequestCycleListener(sessionFactory));
		getApplicationSettings().setAccessDeniedPage( LoginPage.class );
		getSecuritySettings().setAuthorizationStrategy( new FoodNowAuthorizationStrategy() );
		mountPage("/index", IndexPage.class);
		mountPage("/login", LoginPage.class);
		mountPage("/restaurant/show", RestaurantPage.class);
		mountPage("/restaurant/list", RestaurantListPage.class);
		mountPage("/restaurant/add", RestaurantAddPage.class);
		mountPage("/login/update", UpdatePage.class);
		mountPage("/login/changePassword", ChangePasswordPage.class);
		mountPage("/orders", OrderPage.class);
		mountPage("/restaurant/ratings", RatingPage.class);
		mountPage("/restaurant/add", RestaurantAddPage.class);
		mountPage("/restaurant/addDish", AddDishPage.class);
		mountPage("/restaurant/repo", ManagerReportPage.class);
		mountPage("/banuser", BanUserPage.class);
		mountPage("/users/report", UsersReportPage.class);
		mountPage("/order/report", FilterOrderReportPage.class);
		mountPage("/manager/add", AddManagerPage.class);

	}

	@Override
	public Session newSession(Request request, Response response) {
		return new FoodNowSession(request);
	}
	
	@Override
	protected IConverterLocator newConverterLocator() {
		ConverterLocator converterLocator = new ConverterLocator();
		return converterLocator;
	}

}
