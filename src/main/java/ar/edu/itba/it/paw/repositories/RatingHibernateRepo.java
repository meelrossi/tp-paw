package ar.edu.itba.it.paw.repositories;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.edu.itba.it.paw.domain.Rating;

@Repository
public class RatingHibernateRepo extends AbstractHibernateRepo implements RatingRepo {

	@Autowired
	public RatingHibernateRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	public void add(Rating rating) {
		save(rating);
	}
}
