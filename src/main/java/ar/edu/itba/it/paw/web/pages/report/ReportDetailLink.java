package ar.edu.itba.it.paw.web.pages.report;

import java.util.Calendar;

import org.apache.wicket.markup.html.link.Link;

public class ReportDetailLink  extends Link<Void> {

	private static final long serialVersionUID = 1L;

	private int restaurantId;
	private String hood;
	private Calendar date;

	public ReportDetailLink(String id, int restId, String hood, Calendar date) {
		super(id);
		this.restaurantId = restId;
		this.hood = hood;
		this.date = date;
	}

	@Override
	public void onClick() {
		setResponsePage(new ReportDetailPage(restaurantId, hood, date));
	}

}
