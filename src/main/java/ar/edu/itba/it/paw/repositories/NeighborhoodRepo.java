package ar.edu.itba.it.paw.repositories;

import java.util.List;

import ar.edu.itba.it.paw.domain.Neighborhood;

public interface NeighborhoodRepo {

	public List<Neighborhood> getAll();
	
	public List<String> getAllNames();

	public Neighborhood get(int id);
	
	public Neighborhood get(String name);
}
