package ar.edu.itba.it.paw.repositories;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public abstract class AbstractHibernateRepo {
	
	protected final SessionFactory sessionFactory;
	
	public AbstractHibernateRepo(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T get(Class<T> type, Serializable id) {
		return (T) getSession().get(type, id);
	}
	
	
	@SuppressWarnings("unchecked")
	public <T> List<T> find(String hql, Object... params) {
		Query query = getSession().createQuery(hql);
		for (int i = 0; i < params.length; i++) {
			query.setParameter(i, params[i]);
		}
		List<T> list = query.list();
		
		return list;
	}
	
	
	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public Serializable save(Object o) {
		return getSession().save(o);
	}


	@SuppressWarnings("unchecked")
	public <T> void delete(Class<T> type, int id) {
		Session session = getSession();
		T object = (T) session.get(type,id);
	    session.delete(object);
	    session.flush();
	}
}
