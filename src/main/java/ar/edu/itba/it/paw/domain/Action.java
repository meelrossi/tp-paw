package ar.edu.itba.it.paw.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Action extends PersistentEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(nullable = false, length = 20)
	private String name;

	@Column(nullable = false, length = 50)
	private String description;

	@Column(nullable = false, length = 50)
	private String page;

	@Column(nullable = false)
	private Integer user_level;

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public Integer getUser_level() {
		return user_level;
	}

	public void setUser_level(Integer user_level) {
		this.user_level = user_level;
	}

	public Action() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}