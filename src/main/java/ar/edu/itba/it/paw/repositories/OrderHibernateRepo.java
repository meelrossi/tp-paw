package ar.edu.itba.it.paw.repositories;

import java.util.Calendar;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.edu.itba.it.paw.domain.FNOrder;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.domain.User;

@Repository
public class OrderHibernateRepo extends AbstractHibernateRepo implements OrderRepo{

	@Autowired
	public OrderHibernateRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public void add(FNOrder order) {
		save(order);
	}

	public FNOrder find(int id) {
		return get(FNOrder.class, id);
	}

	public List<FNOrder> getAll(User user) {
		return find("from FNOrder where user = ?", user);
	}

	public Integer getOrderQuantity(Restaurant restaurant, Calendar start, Calendar end) {
		List<Order> orders = find("from FNOrder where date >= ? and date <= ? and restaurant = ?", start.getTime(), end.getTime(), restaurant);
		return orders.size();
	}

	public void delete(FNOrder order) {
		super.delete(FNOrder.class, order.getId());		
	}

}
