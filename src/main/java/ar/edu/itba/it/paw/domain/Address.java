package ar.edu.itba.it.paw.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Address extends PersistentEntity {
	
	private static final long serialVersionUID = 1L;

	public final static int STREET_NAME_MAX_LENGTH = 20;

	@Column(nullable = false, length = STREET_NAME_MAX_LENGTH)
	private String street;

	@Column(nullable = false)
	private int number;

	@ManyToOne
	private Neighborhood neighborhood;

	public Address() {

	}

	public Address(String street, int number, Neighborhood neighborhood) {
		super();
		setStreet(street);
		setNumber(number);
		setNeighborhood(neighborhood);
	}
	
	public String toString() {
		return street + " " + number;
	}
	
	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		if (street == null || street.length() == 0 || street.length() > STREET_NAME_MAX_LENGTH) {
			throw new IllegalStateException();
		}
		this.street = street;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		if (number <= 0) {
			throw new IllegalStateException();
		}
		this.number = number;
	}

	public Neighborhood getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(Neighborhood neighborhood) {
		if (neighborhood == null) {
			throw new IllegalStateException();
		}
		this.neighborhood = neighborhood;
	}

}