package ar.edu.itba.it.paw.repositories;

import ar.edu.itba.it.paw.domain.ClosingTerm;

public interface ClosingTermRepo {
	
	public void add(ClosingTerm closingTerm);
}
