package ar.edu.itba.it.paw.web.pages.restaurant.addDish;

import java.util.List;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.RangeValidator;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import ar.edu.itba.it.paw.domain.Dish;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.repositories.DishRepo;
import ar.edu.itba.it.paw.repositories.RestaurantRepo;
import ar.edu.itba.it.paw.web.pages.BasePage;
import ar.edu.itba.it.paw.web.pages.login.FoodNowFeedbackPanel;

public class AddDishPage extends BasePage {

	private static final long serialVersionUID = 1L;

	@SpringBean
	private RestaurantRepo restaurantRepo;

	@SpringBean
	private DishRepo dishRepo;

	private transient List<String> menuTypes;
	private transient String name;
	private transient double price;
	private transient String description;
	private transient String menuType;
	private Integer resId;

	public AddDishPage(final Integer resId) {
		
		this.resId = resId;
		
		Restaurant res = restaurantRepo.get(resId);
		
		add(new FoodNowFeedbackPanel("feedback"));

		Form<AddDishPage> addDishForm = new Form<AddDishPage>("addDishForm",
				new CompoundPropertyModel<AddDishPage>(this)) {

			@Override
			protected void onSubmit() {
				
				Restaurant rest = restaurantRepo.get(resId);
				Dish dish = new Dish(name, description!=null?description:"", price);
				rest.addDish(dish, menuType);
				dishRepo.add(dish);
				restaurantRepo.modify(rest);
				
				success(new StringResourceModel("creationSuccess", this, null).getString());
				
			}
		};

		add(addDishForm);
		menuTypes = restaurantRepo.getAllCategories();
		DropDownChoice<String> ddc = new DropDownChoice<String>("menuTypes",
				new PropertyModel<String>(this, "menuType"), menuTypes);
		addDishForm.add(ddc);
		addDishForm.add(new TextField<String>("name").add(new MaximumLengthValidator(Dish.DISH_NAME_MAX_SIZE)).setRequired(true));
		addDishForm.add(new TextField<Double>("price").add(new RangeValidator(0.0, 9999.9)).setRequired(true));
		addDishForm.add(new TextField<String>("description")
				.add(new MaximumLengthValidator(Dish.DISH_DESCRIPTION_MAX_SIZE)));
		addDishForm.add(new Button("regsubmit", new ResourceModel("submit")));

	}

}
