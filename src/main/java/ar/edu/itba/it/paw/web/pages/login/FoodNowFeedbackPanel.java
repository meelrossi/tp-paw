package ar.edu.itba.it.paw.web.pages.login;

import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.feedback.IFeedbackMessageFilter;
import org.apache.wicket.markup.html.panel.FeedbackPanel;

public class FoodNowFeedbackPanel extends FeedbackPanel {

	private static final long serialVersionUID = 1L;

	public FoodNowFeedbackPanel(String id) {
		super(id);
	}

	public FoodNowFeedbackPanel(String id, IFeedbackMessageFilter filter) {
		super(id, filter);
	}

	@Override
	protected String getCSSClass(FeedbackMessage message) {
		String css;
		switch (message.getLevel()) {
		case FeedbackMessage.SUCCESS:
			css = "alert alert-success fade in";
			break;
		case FeedbackMessage.INFO:
			css = "alert info";
			break;
		case FeedbackMessage.ERROR:
			css = "alert alert-danger fade in";
			break;
		default:
			css = "alert";
		}

		return css;
	}

}
