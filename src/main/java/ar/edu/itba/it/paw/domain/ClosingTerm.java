package ar.edu.itba.it.paw.domain;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class ClosingTerm extends PersistentEntity {
	
	private static final long serialVersionUID = 1L;

	public final static int MOTIVE_MAX_SIZE = 100;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Calendar startDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Calendar endDate;
	
	@Column(nullable = false, length = MOTIVE_MAX_SIZE)
	private String motive;
	
	@ManyToOne
	private Restaurant restaurant;
	
	public ClosingTerm() {
		
	}
	
	public ClosingTerm(Calendar startDate, Calendar endDate, String motive, Restaurant restaurant) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.motive = motive;
		this.restaurant = restaurant;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public Calendar getStartDate() {
		return startDate;
	}

	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}

	public String getMotive() {
		return motive;
	}

	public void setMotive(String motive) {
		this.motive = motive;
	}
}
