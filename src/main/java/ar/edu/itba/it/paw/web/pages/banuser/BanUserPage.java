package ar.edu.itba.it.paw.web.pages.banuser;

import java.util.List;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.domain.Dish;
import ar.edu.itba.it.paw.domain.User;
import ar.edu.itba.it.paw.repositories.UserRepo;
import ar.edu.itba.it.paw.web.pages.BasePage;
import ar.edu.itba.it.paw.web.pages.login.FoodNowFeedbackPanel;
import ar.edu.itba.it.paw.web.pages.restaurant.addDish.AddDishPage;

public class BanUserPage extends BasePage {

	@SpringBean
	private UserRepo userRepo;

	private transient List<String> userList;
	private transient String userEmail;

	public BanUserPage() {

		add(new FoodNowFeedbackPanel("feedback"));

		Form<BanUserPage> banUserForm = new Form<BanUserPage>("banUserForm",
				new CompoundPropertyModel<BanUserPage>(this)) {

			@Override
			protected void onSubmit() {
				User user = userRepo.get(userEmail);
				if (user.isBanned()) {
					success(new StringResourceModel("unBan", this, null).getString());
				} else {
					success(new StringResourceModel("ban", this, null).getString());
				}

				user.setBanned();
			}
		};
		add(banUserForm);
		
		userList = userRepo.getAllUsersMails();
		DropDownChoice<String> ddc = new DropDownChoice<String>("userEmail",
				new PropertyModel<String>(this, "userEmail"), userList);
		banUserForm.add(ddc.setRequired(true));
		
		banUserForm.add(new Button("regsubmit", new ResourceModel("submit")));
	}
}
