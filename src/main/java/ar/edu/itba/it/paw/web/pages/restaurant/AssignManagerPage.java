package ar.edu.itba.it.paw.web.pages.restaurant;

import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.domain.User;
import ar.edu.itba.it.paw.repositories.RestaurantRepo;
import ar.edu.itba.it.paw.repositories.UserRepo;
import ar.edu.itba.it.paw.web.pages.BasePage;
import ar.edu.itba.it.paw.web.pages.login.FoodNowFeedbackPanel;

public class AssignManagerPage extends BasePage {

	private static final long serialVersionUID = 1L;

	@SpringBean
	private UserRepo userRepo;
	
	@SpringBean
	private RestaurantRepo restaurantRepo;

	private transient String managerEmail;

	public AssignManagerPage(int restaurantId) {
		final Restaurant restaurant = restaurantRepo.get(restaurantId);
		
		add(new FoodNowFeedbackPanel("feedback"));
		add(new Label("assignManager", new StringResourceModel("assignManager", this, new Model<Restaurant>(restaurant))));
		
		Form<AssignManagerPage> assignManagerForm = new Form<AssignManagerPage>("assignManagerForm",
				new CompoundPropertyModel<AssignManagerPage>(this)) {
			@Override
			protected void onSubmit() {

				User user = userRepo.get(managerEmail);
				
				restaurant.addManager(user);
				
				user.addRestaurant(restaurant);
				setResponsePage(new RestaurantPage(restaurant.getId()));
			}
		};
		
		
		List<String> managersEmails = userRepo.getAllManagersMails();
		managersEmails.removeAll(userRepo.getAllManagersMails(restaurant));
		
		DropDownChoice<String> ddcUsers = new DropDownChoice<String>("managerEmail",
				new PropertyModel<String>(this, "managerEmail"), managersEmails);
		assignManagerForm.add(ddcUsers.setRequired(true));
		assignManagerForm.add(new Button("updateSubmit", new ResourceModel("submit")));
		
		add(assignManagerForm);
	}
}
