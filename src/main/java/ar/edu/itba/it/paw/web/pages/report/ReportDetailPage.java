package ar.edu.itba.it.paw.web.pages.report;

import java.util.Calendar;

import ar.edu.itba.it.paw.web.pages.BasePage;

public class ReportDetailPage extends BasePage {

	private static final long serialVersionUID = 1L;

	public ReportDetailPage(int restaurantId, String hood, Calendar date) {
		
		add(new ReportDetailListView("reportDetail", restaurantId, hood, date));
	}
}
