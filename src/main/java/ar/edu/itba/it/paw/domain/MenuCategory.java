package ar.edu.itba.it.paw.domain;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class MenuCategory extends PersistentEntity {
	
	private static final long serialVersionUID = 1L;

	public final static int CATEGORY_NAME_MAX_SIZE = 20;

	@OneToMany(mappedBy = "menuCategory", cascade = CascadeType.ALL)
	List<Dish> dishes;

	@Column(nullable = false, length = CATEGORY_NAME_MAX_SIZE)
	String categoryName;

	@ManyToOne
	Restaurant restaurant;

	public MenuCategory() {
	}

	public MenuCategory(String categoryName) {
		setCategoryName(categoryName);
	}

	public MenuCategory(List<Dish> dishes, String categoryName) {
		super();
		setDishes(dishes);
		setCategoryName(categoryName);
	}

	public List<Dish> getDishes() {
		return dishes;
	}
	
	public void setDishes(List<Dish> dishes){
		if (dishes == null) {
			throw new IllegalStateException();
		}
		if (dishes.isEmpty()){
			throw new IllegalArgumentException();
		}
		this.dishes = dishes;
	}

	public void addDish(Dish dish) {
		if (dishes == null) {
			dishes = new LinkedList<Dish>();
		}
		dishes.add(dish);
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		if (categoryName == null || categoryName.length() == 0 || categoryName.length() > CATEGORY_NAME_MAX_SIZE) {
			throw new IllegalStateException();
		}
		this.categoryName = categoryName;
	}

	public void setRestaurant(Restaurant restaurant) {
		if (restaurant == null) {
			throw new IllegalStateException();
		}
		this.restaurant = restaurant;
	}
}