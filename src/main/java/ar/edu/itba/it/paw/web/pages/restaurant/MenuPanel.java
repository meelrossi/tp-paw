package ar.edu.itba.it.paw.web.pages.restaurant;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.domain.Dish;
import ar.edu.itba.it.paw.domain.EntityModel;
import ar.edu.itba.it.paw.domain.EntityResolver;
import ar.edu.itba.it.paw.domain.FNOrder;
import ar.edu.itba.it.paw.domain.MenuCategory;
import ar.edu.itba.it.paw.domain.OrderItem;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.domain.User;
import ar.edu.itba.it.paw.repositories.DishRepo;
import ar.edu.itba.it.paw.repositories.OrderItemRepo;
import ar.edu.itba.it.paw.repositories.OrderRepo;
import ar.edu.itba.it.paw.repositories.UserRepo;
import ar.edu.itba.it.paw.web.FoodNowSession;
import ar.edu.itba.it.paw.web.pages.login.FoodNowFeedbackPanel;
import ar.edu.itba.it.paw.web.pages.orders.OrderConfirmationPage;

public class MenuPanel extends Panel {

	private static final long serialVersionUID = 1L;

	@SpringBean
	private EntityResolver entityResolver;
	
	@SpringBean
	private OrderRepo orderRepo;
	
	@SpringBean
	private DishRepo dishRepo;
	
	@SpringBean
	private OrderItemRepo orderItemRepo;
	
	@SpringBean
	private UserRepo userRepo;
	
	private Form<MenuPanel> orderForm;
	
	private transient Map<String, String> ordersMap;

	public MenuPanel(String id, final IModel<Restaurant> model) {
		super(id);
		add(new FoodNowFeedbackPanel("feedback"));
		final User user = userRepo.get(FoodNowSession.get().getEmail());
		final Restaurant restaurant = model.getObject();
		this.ordersMap = new HashMap<String, String>();
		this.orderForm = new Form<MenuPanel>("orderForm",
				new CompoundPropertyModel<MenuPanel>(this)) {
					private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit() {
				List<OrderItem> dishes = new LinkedList<OrderItem>();
				double total = 0;
				for(Entry<String, String> s: ordersMap.entrySet()) {
					Integer quant = Integer.parseInt(s.getValue());
					if(quant != 0) {
						Dish dish = dishRepo.getDish(Integer.parseInt(s.getKey()));
						total += dish.getPrice()*quant;
						OrderItem orderItem = new OrderItem(dish, quant);
						orderItemRepo.add(orderItem);
						dishes.add(orderItem);
					}
				}
				if(dishes.size() == 0) {
					orderForm.error("Debe haber al menos un plato");
					return;
				}
				if(total < restaurant.getMinAmount()) {
					orderForm.error("El monto debera superar $" + restaurant.getMinAmount());
					return;
				}
				total += restaurant.getDeliveryCost(user);
				FNOrder order = new FNOrder(user, dishes, total, restaurant, Calendar.getInstance().getTime());
				orderRepo.add(order);
				setResponsePage(new OrderConfirmationPage(order));
			}
		};
		add(this.orderForm);
		this.orderForm.add(new Button("createOrderButton").setVisible(restaurant.canMakeOrder(user)));
		initialize(model);
	}

	private void initialize(final IModel<Restaurant> model) {
		this.orderForm.add(new RefreshingView<MenuCategory>("menuCategory") {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<MenuCategory>> getItemModels() {
				List<IModel<MenuCategory>> result = new ArrayList<IModel<MenuCategory>>();
				for (MenuCategory c : model.getObject().getMenu()) {
					final int id = c.getId();
					result.add(new LoadableDetachableModel<MenuCategory>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected MenuCategory load() {
							return entityResolver.fetch(MenuCategory.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<MenuCategory> item) {
				item.add(new Label("categoryName", item.getModelObject().getCategoryName()));
				item.add(new DishPanel("dishPanel", new EntityModel<MenuCategory>(MenuCategory.class, item.getModelObject()), ordersMap));
			}
		});
	}
}
