package ar.edu.itba.it.paw.repositories;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.edu.itba.it.paw.domain.Dish;

@Repository
public class DishHibernateRepo extends AbstractHibernateRepo implements DishRepo{

	@Autowired
	public DishHibernateRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	public Dish getDish(int dishid){
		return get(Dish.class, dishid);
	}

	public void add(Dish dish) {
		save(dish);		
	}
}
