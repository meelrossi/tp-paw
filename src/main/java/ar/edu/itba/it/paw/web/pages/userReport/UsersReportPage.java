package ar.edu.itba.it.paw.web.pages.userReport;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.domain.EntityResolver;
import ar.edu.itba.it.paw.domain.User;
import ar.edu.itba.it.paw.repositories.UserRepo;
import ar.edu.itba.it.paw.web.pages.BasePage;

public class UsersReportPage extends BasePage {
	
	private static final long serialVersionUID = 1L;
	
	@SpringBean
	private UserRepo userRepo;
	
	@SpringBean
	private EntityResolver entityResolver;

	public UsersReportPage() {
		add(new RefreshingView<User>("userRow") {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<User>> getItemModels() {
				List<IModel<User>> result = new ArrayList<IModel<User>>();
				for (User c : userRepo.getAllUsers()) {
					final int id = c.getId();
					result.add(new LoadableDetachableModel<User>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected User load() {
							return entityResolver.fetch(User.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<User> item) {
				User user = item.getModelObject();
				item.add(new Label("userEmail", user.getEmail()));
				item.add(new Label("userName", user.getName() + " " + user.getLastname()));
				SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
				item.add(new Label("lastLogin", formatDate.format(user.getLastLogin().getTime())));
			}
		});
	}
}
