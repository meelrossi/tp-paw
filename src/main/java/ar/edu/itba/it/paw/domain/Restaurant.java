package ar.edu.itba.it.paw.domain;

import java.io.Serializable;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.joda.time.LocalTime;

@Entity
public class Restaurant extends PersistentEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public final static int RESTAURANT_DESCRIPTION_MAX_SIZE = 256;
	public final static int RESTAURANT_NAME_MAX_SIZE = 35;

	@OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL)
	private List<MenuCategory> menu;

	@Column(nullable = false)
	private double minAmount;

	@Column(nullable = false, length = RESTAURANT_DESCRIPTION_MAX_SIZE)
	private String description;

	@Column(nullable = false, length = RESTAURANT_NAME_MAX_SIZE)
	private String name;

	@OneToOne(targetEntity = Address.class, cascade = CascadeType.ALL)
	private Address address;

	@OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL)
	private List<Rating> ratings;

	@OneToOne(targetEntity = DeliveryTime.class, cascade = CascadeType.ALL)
	private DeliveryTime deliveryTime;

	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "restaurants")
	private List<User> managers;

	@OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL)
	private List<FNOrder> orders;

	@OneToMany(cascade = CascadeType.ALL)
	private List<DeliveryNeighborhood> deliveries;
	
	@OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL)
	private List<ClosingTerm> closingTerms;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Calendar creationDate;

	public Restaurant() {
	}
	
	public Restaurant(String name, String description, double minAmount, Address address,
			DeliveryTime deliveryTime) {
		setName(name);
		setDescription(description);
		setMinAmount(minAmount);
		setAddress(address);
		setDeliveryTime(deliveryTime);
		managers = new LinkedList<User>();
		menu = new LinkedList<MenuCategory>();
		ratings = new LinkedList<Rating>();
		this.creationDate = Calendar.getInstance();
	}

	public void addManager(User manager) {
		if (manager == null || manager.getUserLevel() > 1) {
			throw new IllegalStateException();
		}
		managers.add(manager);
	}

	public List<User> getManagers() {
		return managers;
	}

	public List<MenuCategory> getMenu() {
		return menu;
	}

	public void setMenu(List<MenuCategory> menu) {
		if (menu == null) {
			throw new IllegalStateException();
		}
		if (menu.isEmpty()) {
			throw new IllegalArgumentException();
		}
		this.menu = menu;
	}

	public double getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(double minAmount) {
		if (minAmount < 0) {
			throw new IllegalArgumentException();
		}
		this.minAmount = minAmount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if (description.length() > RESTAURANT_DESCRIPTION_MAX_SIZE) {
			throw new IllegalStateException();
		}
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null || name.length() == 0 || name.length() > RESTAURANT_NAME_MAX_SIZE) {
			throw new IllegalStateException();
		}
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		if (address == null) {
			throw new IllegalStateException();
		}
		this.address = address;
	}

	public DeliveryTime getDeliveryTime() {
		return this.deliveryTime;
	}

	public void setDeliveryTime(DeliveryTime deliveryTime) {
		if (deliveryTime == null) {
			throw new IllegalStateException();
		}
		this.deliveryTime = deliveryTime;
	}

	public List<Rating> getAllRatings() {
		return ratings;
	}

	public double getRating() {
		if (ratings.size() == 0) {
			return 0;
		}
		double totalRat = 0;
		for (Rating rat : ratings) {
			totalRat += rat.getRating();
		}
		return totalRat / ratings.size();
	}

	public boolean canRate(User user) {
		if (user == null) {
			return false;
		}
		if (ratings == null) {
			return true;
		}
		for (Rating rat : ratings) {
			if (rat.getUser().equals(user)) {
				return false;
			}
		}
		return true;
	}

	public boolean canAddDish(User user) {
		if (user == null || managers == null) {
			return false;
		}
		if (user.getUserLevel() == 0) {
			return true;
		}
		for (User man : managers) {
			if (man.equals(user)) {
				return true;
			}
		}
		return false;
	}

	public void addRating(Rating rat) {
		if (rat == null){
			throw new IllegalStateException();
		}
		ratings.add(rat);
	}

	public Dish getDish(int id) {
		for (MenuCategory m : menu) {
			for (Dish dish : m.getDishes()) {
				if (dish.getId() == id) {
					return dish;
				}
			}
		}
		return null;
	}

	public List<String> getMenuCategoryNames() {
		List<String> categories = new LinkedList<String>();
		if (menu == null) {
			return null;
		}
		for (MenuCategory menuCategory : menu) {
			categories.add(menuCategory.getCategoryName());
		}
		return categories;
	}

	public void addDish(Dish dish, String menuCategoryName) {
		if (dish == null || menuCategoryName == null || menuCategoryName.length() == 0) {
			throw new IllegalStateException();
		}
		if (menu == null) {
			menu = new LinkedList<MenuCategory>();
		}
		for (MenuCategory menuCategory : menu) {
			if (menuCategory.getCategoryName().equals(menuCategoryName)) {
				dish.setMenuCategory(menuCategory);
				menuCategory.addDish(dish);
				return;
			}
		}
		MenuCategory menuCategory = new MenuCategory(menuCategoryName);
		menuCategory.setRestaurant(this);
		menu.add(menuCategory);
		dish.setMenuCategory(menuCategory);
		menuCategory.addDish(dish);
		return;
	}

	public List<DeliveryNeighborhood> getDeliveries() {
		return deliveries;
	}
	
	public void addDelivery(DeliveryNeighborhood delivery) {
		if (delivery == null) {
			return;
		}
		if (deliveries == null) {
			deliveries = new LinkedList<DeliveryNeighborhood>();
		}
		this.deliveries.add(delivery);
	}

	public boolean canMakeOrder(User user) {
		if (user == null || deliveries == null || getClosingTerm() != null) {
			return false;
		}
		for (DeliveryNeighborhood dn : deliveries) {
			if (dn.getNeighborhood().equals(user.getAddress().getNeighborhood())) {
				return true;
			}
		}
		return false;
	}

	public List<FNOrder> getOrders() {
		return orders;
	}

	public void addOrder(FNOrder order) {
		if (order == null) {
			throw new IllegalStateException();
		}
		this.orders.add(order);
	}

	public List<String> getDeliveryNeighborhoods() {
		List<String> delNe = new LinkedList<String>();
		if (deliveries == null) {
			return null;
		}
		for (DeliveryNeighborhood dn : deliveries) {
			delNe.add(dn.getNeighborhood().getName());
		}
		return delNe;
	}

	public DeliveryNeighborhood getDeliveryNeighborhoods(String name) {
		if (deliveries == null || name == null) {
			return null;
		}
		for (DeliveryNeighborhood dn : deliveries) {
			if (dn.getNeighborhood().getName().equals(name)) {
				return dn;
			}
		}
		return null;
	}

	public Double getDeliveryCost(User user) {
		if(user == null || user.getAddress() == null || user.getAddress().getNeighborhood() == null) {
			return null;
		}
		if(deliveries == null) {
			return null;
		}
		for(DeliveryNeighborhood dn:deliveries) {
			if(dn.getNeighborhood().equals(user.getAddress().getNeighborhood())) {
				return dn.getDeliveryCost();
			}
		}
		return null;
	}
	
	public String getDeliveryCostString(User user) {
		if(user == null || user.getAddress() == null || user.getAddress().getNeighborhood() == null) {
			return "";
		}
		if(deliveries == null) {
			return "Este restoran no realiza pedidos a tu barrio.";
		}
		for(DeliveryNeighborhood dn:deliveries) {
			if(dn.getNeighborhood().equals(user.getAddress().getNeighborhood())) {
				return "$" + String.valueOf(dn.getDeliveryCost());
			}
		}
		return "Este restoran no realiza pedidos a tu barrio.";
	}
	
	public boolean canHaveDeliveries() {
		LocalTime currentTime = LocalTime.now();
		LocalTime startTime = new LocalTime(deliveryTime.getOpeningTime());
		LocalTime endTime = new LocalTime(deliveryTime.getClosingTime());
		if(endTime.isBefore(startTime)) {
			return currentTime.isBefore(endTime) || currentTime.isAfter(startTime);
		} else {
			return currentTime.isBefore(startTime) || currentTime.isAfter(startTime);
		}
	}
	
	public void setDeliveries(List<DeliveryNeighborhood> deliveries) {
		this.deliveries = deliveries;
	}
	public List<String> getAllManagerMails() {
		List<String> restManagers = new LinkedList<String>();
		if(this.managers == null) {
			return restManagers;
		}
		for(User user : managers) {
			restManagers.add(user.getEmail());
		}
		return restManagers;
	}
	
	public void addClosingTerm(ClosingTerm closingTerm) {
		if(closingTerm == null) {
			throw new IllegalStateException();
		}
		if(closingTerms == null) {
			closingTerms = new LinkedList<ClosingTerm>();
		}
		for(ClosingTerm c: closingTerms) {
			if(between(closingTerm.getStartDate(), c.getStartDate(), c.getEndDate()) ||  between(closingTerm.getEndDate(), c.getStartDate(), c.getEndDate())) {
				throw new IllegalStateException();
			}
		}
		closingTerms.add(closingTerm);
	}
	
	public ClosingTerm getClosingTerm() {
		Calendar today = Calendar.getInstance();
		for(ClosingTerm c: closingTerms) {
			if(between(today, c.getStartDate(), c.getEndDate())) {
				return c;
			}
		}
		return null;
	}
	
	public boolean between(Calendar date, Calendar startDate, Calendar endDate){
		return startDate.before(date) && date.before(endDate);
	}
}