package ar.edu.itba.it.paw.repositories;

import ar.edu.itba.it.paw.domain.DeliveryNeighborhood;

public interface DeliveryNeighborhoodRepo {
	
	public DeliveryNeighborhood getNeighborhood(int id);
	
	public void add(DeliveryNeighborhood n);
	

}
