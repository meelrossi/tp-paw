package ar.edu.itba.it.paw.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class ChangePasswordProperty extends PersistentEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column
	private int days;
	
	public int getDays() {
		return days;
	}

}
