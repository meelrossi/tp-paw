package ar.edu.itba.it.paw.repositories;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.edu.itba.it.paw.domain.Neighborhood;

@Repository
public class NeighborhoodHibernateRepo extends AbstractHibernateRepo implements NeighborhoodRepo{
	
	@Autowired
	public NeighborhoodHibernateRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public List<Neighborhood> getAll() {
		return find("from Neighborhood");
	}

	public Neighborhood get(int id) {
		return get(Neighborhood.class, id);
	}
	
	public Neighborhood get(String name) {
		List<Neighborhood> neighborhoods = find("from Neighborhood where name = ?", name);
		if(neighborhoods != null && neighborhoods.size() == 1) {
			return neighborhoods.get(0);
		}
		return null;
	}

	public List<String> getAllNames() {
		List<String> names = new LinkedList<String>();
		for(Neighborhood nb : getAll()) {
			names.add(nb.getName());
		}
		return names;
	}

}
