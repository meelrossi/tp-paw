package ar.edu.itba.it.paw.web.pages;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.domain.Action;
import ar.edu.itba.it.paw.domain.EntityResolver;
import ar.edu.itba.it.paw.repositories.ActionRepo;
import ar.edu.itba.it.paw.web.FoodNowSession;
import ar.edu.itba.it.paw.web.pages.index.IndexPage;
import ar.edu.itba.it.paw.web.pages.login.LoginPage;

@SuppressWarnings("deprecation")
public class ActionsPanel extends Panel {

	private static final long serialVersionUID = 1L;

	@SpringBean
	private EntityResolver entityResolver;
	
	@SpringBean 
	private ActionRepo actionRepo;

	@SuppressWarnings({ "rawtypes", "serial" })
	public ActionsPanel(String id) {
		super(id);
		initialize();
		boolean isSignIn = FoodNowSession.get().isSignedIn();
		Link logoutLink = new Link("logoutLink") {

			@Override
			public void onClick() {
				FoodNowSession.get().invalidate();
				setResponsePage(IndexPage.class);
			}
		};
		add(logoutLink.setVisible(isSignIn));

		Link loginLink = new Link("loginLink") {

			@Override
			public void onClick() {
				setResponsePage(LoginPage.class);
			}
		};
		add(loginLink.setVisible(!isSignIn));
	}

	private void initialize() {
		final List<Action> actions = actionRepo.getActions(FoodNowSession.get().getUserLvl(), "header");
		add(new RefreshingView<Action>("action") {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<Action>> getItemModels() {
				List<IModel<Action>> result = new ArrayList<IModel<Action>>();
				for (Action c : actions) {
					final int id = c.getId();
					result.add(new LoadableDetachableModel<Action>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected Action load() {
							return entityResolver.fetch(Action.class, id);
						}
					});
				}
				return result.iterator();
			}

			@SuppressWarnings({ "rawtypes" })
			@Override
			protected void populateItem(Item<Action> item) {
				Link actionLink = (Link) new Link("actionLink") {
					private static final long serialVersionUID = 1L;

					@Override
					public void onClick() {
					}
				}.add(new Label("label", item.getModelObject().getDescription())).add(new SimpleAttributeModifier("href", item.getModelObject().getName()));
				item.add(actionLink);
			}
		});
	}
}
