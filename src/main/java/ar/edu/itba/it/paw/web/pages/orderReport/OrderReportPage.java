package ar.edu.itba.it.paw.web.pages.orderReport;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.domain.EntityResolver;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.repositories.OrderRepo;
import ar.edu.itba.it.paw.repositories.RestaurantRepo;
import ar.edu.itba.it.paw.repositories.UserRepo;
import ar.edu.itba.it.paw.web.pages.BasePage;

public class OrderReportPage extends BasePage {
	
	@SpringBean
	private OrderRepo orderRepo;
	
	@SpringBean
	private RestaurantRepo restaurantRepo;
	
	@SpringBean
	private UserRepo userRepo;
	
	@SpringBean
	private EntityResolver entityResolver;
	
	private transient Calendar start;
	private transient Calendar end;
	
	public OrderReportPage(Calendar s, Calendar e) {
		this.start = s;
		this.end = e;
		add(new RefreshingView<Restaurant>("restaurantRow") {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<Restaurant>> getItemModels() {
				List<IModel<Restaurant>> result = new ArrayList<IModel<Restaurant>>();
				for (Restaurant r : restaurantRepo.getAll()) {
					final int id = r.getId();
					result.add(new LoadableDetachableModel<Restaurant>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected Restaurant load() {
							return entityResolver.fetch(Restaurant.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<Restaurant> item) {
				Restaurant rest = item.getModelObject();
				item.add(new Label("restaurantName", rest.getName()));
				item.add(new Label("amount", orderRepo.getOrderQuantity(rest, start, end).toString()));
				
			}
		});

	}
}
