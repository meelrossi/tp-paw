package ar.edu.itba.it.paw.repositories;

import ar.edu.itba.it.paw.domain.Rating;

public interface RatingRepo {
	
	public void add(Rating rat);
}
