package ar.edu.itba.it.paw.web.pages.restaurant.addRestaurant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.RangeValidator;

import ar.edu.itba.it.paw.domain.DeliveryNeighborhood;
import ar.edu.itba.it.paw.domain.EntityResolver;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.repositories.DeliveryNeighborhoodRepo;
import ar.edu.itba.it.paw.repositories.RestaurantRepo;
import ar.edu.itba.it.paw.web.pages.login.FoodNowFeedbackPanel;

public class NeighbourhoodPricePanel extends Panel {

	private static final long serialVersionUID = 1L;

	@SpringBean
	private RestaurantRepo restaurantRepo;

	@SpringBean
	private DeliveryNeighborhoodRepo deliveryNeighborhoodRepo;

	@SpringBean
	private EntityResolver entityResolver;

	private transient Map<String, String> neighborhoodMap;
	private transient double cost;

	private Form<NeighbourhoodPricePanel> priceForm;

	public NeighbourhoodPricePanel(String id, final IModel<Restaurant> model) {
		super(id);
		final Restaurant restaurant = model.getObject();
		this.neighborhoodMap = new HashMap<String, String>();
		add(new FoodNowFeedbackPanel("feedback"));
		this.priceForm = new Form<NeighbourhoodPricePanel>("priceForm",
				new CompoundPropertyModel<NeighbourhoodPricePanel>(this)) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit() {
				for (Entry<String, String> s : neighborhoodMap.entrySet()) {
					DeliveryNeighborhood n = deliveryNeighborhoodRepo.getNeighborhood(Integer.parseInt(s.getKey()));
					Integer cost = Integer.parseInt(s.getValue());
					n.setDeliveryCost(cost);
					deliveryNeighborhoodRepo.add(n);
				}
				success(new StringResourceModel("creationSuccess", this, null).getString());
			}
		};
		add(this.priceForm);
		this.priceForm.add(new Button("addPricesButton"));
		initialize(model);
	}

	private void initialize(final IModel<Restaurant> model) {
		this.priceForm.add(new RefreshingView<DeliveryNeighborhood>("neighbourhoodList") {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<DeliveryNeighborhood>> getItemModels() {
				List<IModel<DeliveryNeighborhood>> result = new ArrayList<IModel<DeliveryNeighborhood>>();
				for (DeliveryNeighborhood n : model.getObject().getDeliveries()) {
					final int id = n.getId();
					result.add(new LoadableDetachableModel<DeliveryNeighborhood>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected DeliveryNeighborhood load() {
							return entityResolver.fetch(DeliveryNeighborhood.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<DeliveryNeighborhood> item) {
				item.add(new Label("neighborhoodName", item.getModelObject().getNeighborhood().getName()));
				NumberTextField<Integer> neighborhoodPrice = (NumberTextField<Integer>) new NumberTextField<Integer>("cost",
						new PropertyModel<Integer>(neighborhoodMap, Integer.toString(item.getModelObject().getId()))).add(new SimpleAttributeModifier("min", String
								.valueOf(0)));
				item.add(neighborhoodPrice);
			}
		});

	}

}
