package ar.edu.itba.it.paw.web.pages.login;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;
import org.apache.wicket.validation.validator.StringValidator.MinimumLengthValidator;

import ar.edu.itba.it.paw.domain.User;
import ar.edu.itba.it.paw.repositories.UserRepo;
import ar.edu.itba.it.paw.web.FoodNowSession;
import ar.edu.itba.it.paw.web.pages.BasePage;
import ar.edu.itba.it.paw.web.pages.index.IndexPage;

public class ChangePasswordPage extends BasePage {

	@SpringBean
	private UserRepo userRepo;

	private transient String newPassword;

	public ChangePasswordPage() {

		add(new FoodNowFeedbackPanel("feedback"));

		Form<ChangePasswordPage> changePasswordForm = new Form<ChangePasswordPage>("changePasswordForm",
				new CompoundPropertyModel<ChangePasswordPage>(this)) {
			
			@Override
			protected void onSubmit() {
				User user = userRepo.get(FoodNowSession.get().getEmail());
				if (user.checkPassword(newPassword)) {
					error(new StringResourceModel("wrongPassword", this, null).getString());
					return;
				} else {
					user.setPassword(newPassword);
					userRepo.add(user);
					success(new StringResourceModel("success", this, null).getString());
					userRepo.add(user);
					setResponsePage(IndexPage.class);
				}
			}
		};
		add(changePasswordForm);

		changePasswordForm.add(new PasswordTextField("newPassword").setRequired(true)
				.add(new MaximumLengthValidator(User.PASSWORD_MAX_SIZE)).add(new MinimumLengthValidator(6)));

		changePasswordForm.add(new Button("regsubmit", new ResourceModel("submit")));
	}

}
