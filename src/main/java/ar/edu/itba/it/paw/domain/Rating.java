package ar.edu.itba.it.paw.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Rating extends PersistentEntity {
	
	private static final long serialVersionUID = 1L;

	public final static int RATING_DESCRIPTION_MAX_SIZE = 256;
	
	@ManyToOne
	private User user;
	
	@Column(nullable = false, length = RATING_DESCRIPTION_MAX_SIZE)
	private String description;
	
	@Column(nullable = false)
	private double rating;
	
	@ManyToOne
	private Restaurant restaurant;

	public Rating() {

	}
	
	public Rating(User user, Restaurant restaurant, String description, double rating) {
		setUser(user);
		setDescription(description);
		setRating(rating);
		setRestaurant(restaurant);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		if (user == null) {
			throw new IllegalStateException();
		}
		this.user = user;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if (description == null || description.length() == 0 || description.length() > RATING_DESCRIPTION_MAX_SIZE) {
			throw new IllegalStateException();
		}
		this.description = description;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		if (rating < 0 || rating > 5) {
			throw new IllegalStateException();
		}
		this.rating = rating;
	}
	
	public Restaurant getRestaurant() {
		return restaurant;
	}
	
	public void setRestaurant(Restaurant restaurant) {
		if (restaurant == null) {
			throw new IllegalStateException();
		}
		this.restaurant = restaurant;
	}

}