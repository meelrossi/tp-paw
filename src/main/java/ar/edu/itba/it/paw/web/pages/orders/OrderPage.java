package ar.edu.itba.it.paw.web.pages.orders;

import ar.edu.itba.it.paw.web.pages.BasePage;

public class OrderPage extends BasePage {

	private static final long serialVersionUID = 1L;

	public OrderPage() {
		add(new OrderPanel("orderPanel"));
	}
}
