package ar.edu.itba.it.paw.web.pages.orders;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.StringResourceModel;

import ar.edu.itba.it.paw.domain.FNOrder;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.web.pages.BasePage;

public class OrderConfirmationPage extends BasePage {

	private static final long serialVersionUID = 1L;
	
	public OrderConfirmationPage(FNOrder order) {
		add(new Label("address", order.getUser().getAddress().getStreet() + " " + order.getUser().getAddress().getNumber()));
		add(new Label("total", "$ " + Double.toString(order.getTotal())));
		add(new Label("successfulOrder", new StringResourceModel("successfulOrder", this, new Model<Restaurant>(order.getRestaurant()))));
	}

}
