package ar.edu.itba.it.paw.repositories;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.domain.User;

@Repository
public class UserHibernateRepo extends AbstractHibernateRepo implements UserRepo {

	@Autowired
	public UserHibernateRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public User get(String email) {
		if(email == null) {
			return null;
		}
		List<User> users = find("select user from User user where user.email = ?", email);
		if (users.isEmpty()) {
			return null;
		}
		return users.get(0);
	}

	public void add(User user) {
		save(user.getAddress());
		save(user);
	}

	public User get(Integer id) {
		if (id == null) {
			return null;
		}
		return get(User.class, id);
	}

	public List<User> getAllUsers() {
		return find("FROM User");
	}

	public List<String> getAllUsersMails() {
		List<String> mails = new LinkedList<String>();
		List<User> users = getAllUsers();
		for (User user : users) {
			if (user.getUserLevel() == 2) {
				mails.add(user.getEmail());
			}
		}
		return mails;
	}

	public void delete(User user) {
		delete(User.class, user.getId());
	}

	public List<String> getAllManagersMails() {
		List<String> mails = new LinkedList<String>();
		List<User> users = getAllUsers();
		for (User user : users) {
			if (user.getUserLevel() < 2) {
				mails.add(user.getEmail());
			}
		}
		return mails;
	}

	public List<String> getAllUserNames() {
		List<String> names = new LinkedList<String>();
		List<User> users = getAllUsers();
		for (User user : users) {
			names.add(user.getName() + " " + user.getLastname());
		}
		return names;
	}

	public List<String> getAllManagersMails(Restaurant restaurant) {
		List<User> users = getAllUsers();
		List<String> managers = new LinkedList<String>();
		for(User user: users) {
			if(user.getRestaurants().contains(restaurant)) {
				managers.add(user.getEmail());
			}
		}
		return managers;
	}

}
