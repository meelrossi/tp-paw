package ar.edu.itba.it.paw.web.pages.restaurant;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Transient;

import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.domain.Action;
import ar.edu.itba.it.paw.domain.EntityResolver;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.repositories.ActionRepo;
import ar.edu.itba.it.paw.repositories.RestaurantRepo;
import ar.edu.itba.it.paw.web.FoodNowSession;
import ar.edu.itba.it.paw.web.pages.BasePage;

@SuppressWarnings({ "unchecked", "serial", "rawtypes", "deprecation" })
public class RestaurantListPage extends BasePage {
	// TODO: ver el rating de los restoranes.

	private static final long serialVersionUID = 1L;

	@SpringBean
	private RestaurantRepo restaurantRepo;

	@SpringBean
	private ActionRepo actionsRepo;

	@SpringBean
	private EntityResolver entityResolver;

	@Transient
	private List<Action> availableActions;

	public RestaurantListPage() {
		availableActions = actionsRepo.getActions( FoodNowSession.get().getUserLvl(), "restaurant/list");
		initialize();
	};

	private String[] selectedCategories;

	private void initialize() {

		add(new RefreshingView<Restaurant>("restaurants") {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<Restaurant>> getItemModels() {
				List<IModel<Restaurant>> result = new ArrayList<IModel<Restaurant>>();

				List<Restaurant> restaurants;
				if (selectedCategories != null && selectedCategories.length > 0) {
					restaurants = restaurantRepo.getAll(selectedCategories);
				} else {
					restaurants = restaurantRepo.getAll();
				}

				for (Restaurant a : restaurants) {
					final int id = a.getId();
					result.add(new LoadableDetachableModel<Restaurant>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected Restaurant load() {
							return entityResolver.fetch(Restaurant.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<Restaurant> item) {
				item.add(new RestaurantLink("restaurantLink", item
						.getModelObject().getId()));
				item.add(new RestaurantLink("menuLink", item.getModelObject()
						.getId()));
				item.add(new Label("name", item.getModelObject().getName()));
				item.add(new Label("addressStreet", item.getModelObject()
						.getAddress().getStreet()));
				item.add(new Label("addressNumber", String.valueOf(item
						.getModelObject().getAddress().getNumber())));
				item.add(new NumberTextField("rating")
						.add(new SimpleAttributeModifier("value", String
								.valueOf(item.getModelObject().getRating()))));

				int restId = item.getModelObject().getId();
				RepeatingView actions = new RepeatingView("actions");
				
				if (availableActions != null) {
					for (Action ac : availableActions) {
						actions.add(new ActionLink(actions.newChildId(), ac
								.getName(), restId).add(
								new SimpleAttributeModifier("title", ac
										.getDescription())).add(
								new SimpleAttributeModifier("class", ac
										.getName())));
					}
				}
				item.add(actions);

			}
		});

		add(new CategoriesCheckboxForm("categoriesCheckboxForm"));
	}

	private class CategoriesCheckboxForm extends Form {
		private List<CategoryWrapper> categories;

		public CategoriesCheckboxForm(String name) {
			super(name);

			categories = new ArrayList<CategoryWrapper>();
			for (String cat : restaurantRepo.getAllCategories()) {
				categories.add(new CategoryWrapper(cat));
			}

			ListView listView = new ListView("list", categories) {
				@Override
				protected void populateItem(ListItem item) {
					CategoryWrapper wrapper = (CategoryWrapper) item
							.getModelObject();
					item.add(new Label("name", wrapper.getName()));
					item.add(new CheckBox("check", new PropertyModel(wrapper,
							"selected")));
				}
			};
			listView.setReuseItems(true);
			add(listView);
		}

		public void onSubmit() {
			List<String> selectedCategoriesList = new ArrayList<String>();
			for (CategoryWrapper cat : categories) {
				if (cat.getSelected())
					selectedCategoriesList.add(cat.getName());
			}
			selectedCategories = new String[selectedCategoriesList.size()];
			selectedCategoriesList.toArray(selectedCategories);

		}
	}

}
