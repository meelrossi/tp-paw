package ar.edu.itba.it.paw.repositories;

import java.util.Calendar;
import java.util.List;

import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.domain.User;

public interface RestaurantRepo {

	public Restaurant get(int id);

	public void delete(int restaurantId);

	public void modify(Restaurant res);

	public void add(Restaurant res);

	public List<Restaurant> getAll();

	public List<Restaurant> getAll(String[] categories);

	public List<Restaurant> mostPopular();
	
	public List<String> getAllCategories();
	
	public boolean exists(String restaurantName);
	
	public List<Restaurant> newRestaurants(User user);
}
