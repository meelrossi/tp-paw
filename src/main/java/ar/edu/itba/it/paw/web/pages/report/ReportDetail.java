package ar.edu.itba.it.paw.web.pages.report;

import java.io.Serializable;
import java.util.Calendar;

public class ReportDetail implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Calendar date;
	private int cant;
	private int hours;
	
	public ReportDetail(Calendar date, int cant, int hours) {
		super();
		this.date = date;
		this.cant = cant;
		this.hours = hours;
	}
	public Calendar getDate() {
		return date;
	}
	public void setDate(Calendar date) {
		this.date = date;
	}
	public int getCant() {
		return cant;
	}
	public void setCant(int cant) {
		this.cant = cant;
	}
	public int getHours() {
		return hours;
	}
	public void setHours(int hours) {
		this.hours = hours;
	}
	
	

}
