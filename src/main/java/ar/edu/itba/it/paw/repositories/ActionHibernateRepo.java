package ar.edu.itba.it.paw.repositories;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.edu.itba.it.paw.domain.Action;

@Repository
public class ActionHibernateRepo extends AbstractHibernateRepo implements ActionRepo{
	
	@Autowired
	public ActionHibernateRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public List<Action> getActions(int userLevel, String pageName) {
		
		List<Action> list = find("from Action where page=? and user_level >= ?", pageName, userLevel);
		
		return list;
	}

}
