package ar.edu.itba.it.paw.repositories;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.edu.itba.it.paw.domain.ChangePasswordProperty;

@Repository
public class ChangePasswordPropertyHibernateRepo extends AbstractHibernateRepo implements ChangePasswordPropertyRepo {

	@Autowired
	public ChangePasswordPropertyHibernateRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public int getPasswordPropery() {
		List<ChangePasswordProperty> list = find("from ChangePasswordProperty");
		ChangePasswordProperty item = list.get(0);
		return item.getDays();
	}

}
