package ar.edu.itba.it.paw.web.pages.restaurant;

import org.apache.wicket.markup.html.basic.Label;

import ar.edu.itba.it.paw.domain.EntityModel;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.web.pages.BasePage;

public class RatingPage extends BasePage{

	private static final long serialVersionUID = 1L;

	public RatingPage(Restaurant restaurant) {
		add(new Label("restaurantName", restaurant.getName()));
		add(new RatingPanel("ratingPanel", new EntityModel<Restaurant>(Restaurant.class, restaurant)));
	}
}
