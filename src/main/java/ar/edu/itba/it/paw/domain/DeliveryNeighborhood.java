package ar.edu.itba.it.paw.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class DeliveryNeighborhood extends PersistentEntity{
	
	private static final long serialVersionUID = 1L;

	@ManyToOne
	private Neighborhood neighborhood;
	
	@Column(nullable = false)
	private double deliveryCost;

	public DeliveryNeighborhood() {

	}
	
	public DeliveryNeighborhood(Neighborhood neighborhood) {
		setNeighborhood(neighborhood);
	}

	public DeliveryNeighborhood(Neighborhood neighborhood, double deliveryCost) {
		setNeighborhood(neighborhood);
		setDeliveryCost(deliveryCost);
	}

	public Neighborhood getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(Neighborhood neighborhood) {
		if (neighborhood == null) {
			throw new IllegalStateException();
		}
		this.neighborhood = neighborhood;
	}

	public double getDeliveryCost() {
		return deliveryCost;
	}

	public void setDeliveryCost(double deliveryCost) {
		if (deliveryCost < 0) {
			throw new IllegalStateException();
		}
		this.deliveryCost = deliveryCost;
	}
}
