package ar.edu.itba.it.paw.repositories;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.edu.itba.it.paw.domain.ClosingTerm;

@Repository
public class ClosingTermHibernateRepo extends AbstractHibernateRepo implements ClosingTermRepo{
	
	@Autowired
	public ClosingTermHibernateRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	public void add(ClosingTerm closingTerm) {
		save(closingTerm);
	}

}
