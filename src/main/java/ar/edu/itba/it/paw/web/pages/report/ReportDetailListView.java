package ar.edu.itba.it.paw.web.pages.report;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.domain.FNOrder;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.repositories.RestaurantRepo;

public class ReportDetailListView extends RefreshingView<ReportDetail> {

	private int restId;
	@SuppressWarnings("unused")
	private String hood;
	private Calendar date;

	@SpringBean
	private RestaurantRepo restaurantRepo;

	public ReportDetailListView(String id, int restId, String hood,
			Calendar date) {
		super(id);
		this.restId = restId;
		this.hood = hood;
		this.date = date;
	}

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("deprecation")
	@Override
	protected Iterator<IModel<ReportDetail>> getItemModels() {
		List<IModel<ReportDetail>> result = new ArrayList<IModel<ReportDetail>>();

		Restaurant rest = restaurantRepo.get(restId);

		Map<Integer, ReportDetail> orders = new HashMap<Integer, ReportDetail>();

		for (FNOrder order : rest.getOrders()) {
			if (order.getDate().getYear() == date.getTime().getYear()
					&& order.getDate().getMonth() == date.getTime().getMonth()
					&& order.getDate().getDay() == date.getTime().getDay()) {

				if (orders.containsKey(order.getDate().getHours())) {
					orders.get(order.getDate().getHours())
							.setCant(
									orders.get(order.getDate().getHours())
											.getCant() + 1);
				} else {
					orders.put(order.getDate().getHours(), new ReportDetail(
							date, 1, order.getDate().getHours()));
				}
			}
		}
		
		for (ReportDetail r : orders.values()) {
			result.add(new ReportDetailModel(r));
		}

		return result.iterator();
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void populateItem(Item<ReportDetail> item) {
		ReportDetail reportDetail = item.getModelObject();

		item.add(new Label("hour", String.valueOf(reportDetail.getHours())));

		item.add(new Label("cant", String.valueOf(reportDetail.getCant())));

	}

	@SuppressWarnings("serial")
	private class ReportDetailModel extends
			LoadableDetachableModel<ReportDetail> {

		private ReportDetail report;

		public ReportDetailModel(ReportDetail report) {
			this.report = report;
		}

		@Override
		protected ReportDetail load() {
			return report;
		}

	}

}
