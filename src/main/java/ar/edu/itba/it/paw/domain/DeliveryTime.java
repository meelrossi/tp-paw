package ar.edu.itba.it.paw.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class DeliveryTime extends PersistentEntity{
	
	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	private String openingTime;
	
	@Column(nullable = false)
	private String closingTime;
	
	public DeliveryTime() {
		
	}

	public DeliveryTime(String openingTime, String closingTime) {
		this.openingTime = openingTime;
		this.closingTime = closingTime;
	}

	public String getOpeningTime() {
		return openingTime;
	}

	public void setOpeningTime(String openingTime) {
		if (openingTime == null || openingTime.length() == 0) {
			throw new IllegalStateException();
		}
		this.openingTime = openingTime;
	}

	public String getClosingTime() {
		return closingTime;
	}

	public void setClosingTime(String closingTime) {
		if (closingTime == null || closingTime.length() == 0) {
			throw new IllegalStateException();
		}
		this.closingTime = closingTime;
	}

	public String getDescription() {
		return "Disponible desde " + openingTime + " hasta " + closingTime;
	}

}
