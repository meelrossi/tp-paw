package ar.edu.itba.it.paw.repositories;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.edu.itba.it.paw.domain.DeliveryNeighborhood;
import ar.edu.itba.it.paw.domain.MenuCategory;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.domain.User;

@Repository
public class RestaurantHibernateRepo extends AbstractHibernateRepo implements RestaurantRepo {

	@Autowired
	public RestaurantHibernateRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public Restaurant get(int id) {
		return get(Restaurant.class, id);
	}

	public void delete(int restaurantId) {
		delete(Restaurant.class, restaurantId);
	}

	public void modify(Restaurant res) {
		save(res.getAddress());
		save(res);
	}

	public void add(Restaurant res) {
		save(res.getAddress());
		save(res);
	}

	public List<Restaurant> getAll() {
		return find("FROM Restaurant");
	}

	public List<Restaurant> getAll(String[] categories) {
		List<Restaurant> restaurants = getAll();
		if (categories == null) {
			return restaurants;
		}
		List<Restaurant> filteredRestaurants = new LinkedList<Restaurant>();
		boolean shouldBreak = false;
		for (Restaurant res : restaurants) {
			for (MenuCategory menu : res.getMenu()) {
				for (String cat : categories) {
					if (cat.equals(menu.getCategoryName())) {
						filteredRestaurants.add(res);
						shouldBreak = true;
						break;
					}
				}
				if (shouldBreak) {
					shouldBreak = false;
					break;
				}
			}
		}
		return filteredRestaurants;
	}

	@SuppressWarnings("unchecked")
	public List<String> getAllCategories() {
		return sessionFactory.getCurrentSession().createCriteria(MenuCategory.class)
				.setProjection(Projections.distinct(Projections.property("categoryName"))).list();
	}

	public List<Restaurant> mostPopular() {
		List<Restaurant> restaurants = find("from Restaurant");
		Collections.sort(restaurants, new Comparator<Restaurant>() {
			public int compare(Restaurant r1, Restaurant r2) {
				if (r1.getOrders().size() < r2.getOrders().size()) {
					return 1;
				} else if (r1.getOrders().size() > r2.getOrders().size()) {
					return -1;
				}
				return 0;
			}
		});
		int size = restaurants.size();
		if (size < 4) {
			return restaurants.subList(0, size);
		}
		return restaurants.subList(0, 4);
	}
	
	public boolean exists(String restaurantName) {
		List<Restaurant> restaurants = find("from Restaurant where name = ?", restaurantName);
		return !restaurants.isEmpty();
	}

	public List<Restaurant> newRestaurants(User user) {
		List<Restaurant> result = new LinkedList<Restaurant>();
		if (user != null) {
			List<Restaurant> restaurants = find("from Restaurant where creationDate > ?", user.getPreviousLogin());
			for (Restaurant r : restaurants) {
				for (DeliveryNeighborhood dn : r.getDeliveries()) {
					if (dn.getNeighborhood().equals(user.getAddress().getNeighborhood())) {
						result.add(r);
					}
				}
			}
		}
		return result;
	}
}