package ar.edu.itba.it.paw.repositories;

import ar.edu.itba.it.paw.domain.DeliveryTime;

public interface DeliveryTimeRepo {

	public DeliveryTime getDeliveryTime(int id);
}
