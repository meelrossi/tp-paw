package ar.edu.itba.it.paw.repositories;

import java.util.List;

import ar.edu.itba.it.paw.domain.Action;

public interface ActionRepo {
	
	public List<Action> getActions(int userLevel, String pageName);

}
