package ar.edu.itba.it.paw.web.pages.report;

import java.io.Serializable;
import java.util.Calendar;



public class Report implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Calendar date;
	private String restaurantName;
	private int cantOrders;
	private String hood;
	private int restaurantId;
		
	public Report(Calendar date, String restaurantName, int cantOrders,
			String hood, int restaurantId) {
		super();
		this.date = date;
		this.restaurantName = restaurantName;
		this.cantOrders = cantOrders;
		this.hood = hood;
		this.restaurantId = restaurantId;
	}
	public int getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(int restaurantId) {
		this.restaurantId = restaurantId;
	}
	public Calendar getDate() {
		return date;
	}
	public void setDate(Calendar date) {
		this.date = date;
	}
	public String getRestaurantName() {
		return restaurantName;
	}
	public void setRestaurantName(String restaurantName) {
		this.restaurantName = restaurantName;
	}
	public int getCantOrders() {
		return cantOrders;
	}
	public void setCantOrders(int cantOrders) {
		this.cantOrders = cantOrders;
	}
	public String getHood() {
		return hood;
	}
	public void setHood(String hood) {
		this.hood = hood;
	}
	
	

}
