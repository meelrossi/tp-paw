package ar.edu.itba.it.paw.web.pages.restaurant;

import org.apache.wicket.markup.html.link.Link;

public class RestaurantLink extends Link<Void> {
	
	private static final long serialVersionUID = 1L;
	
	private int restaurantId;
	public RestaurantLink(String id, int restId) {
		super(id);
		restaurantId = restId;
	}

	@Override
	public void onClick() {
		setResponsePage(new RestaurantPage(restaurantId));
	}

}
