package ar.edu.itba.it.paw.repositories;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.edu.itba.it.paw.domain.DeliveryTime;

public class DeliveryTimeHibernateRepo extends AbstractHibernateRepo implements DeliveryTimeRepo {

	@Autowired
	public DeliveryTimeHibernateRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	
	public DeliveryTime getDeliveryTime(int id) {
		return get(DeliveryTime.class, id);
	}
}
