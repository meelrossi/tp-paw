package ar.edu.itba.it.paw.repositories;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.edu.itba.it.paw.domain.OrderItem;

@Repository
public class OrderItemHibernateRepo extends AbstractHibernateRepo implements OrderItemRepo{
	
	@Autowired
	public OrderItemHibernateRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	public void add(OrderItem orderItem) {
		save(orderItem);
	}

}
