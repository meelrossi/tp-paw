package ar.edu.itba.it.paw.repositories;

import java.util.Calendar;
import java.util.List;


import ar.edu.itba.it.paw.domain.FNOrder;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.domain.User;

public interface OrderRepo {
	
	public void add(FNOrder order);

	public FNOrder find(int id);
	
	public List<FNOrder> getAll(User user);
	
	public Integer getOrderQuantity(Restaurant restaurant, Calendar start, Calendar end);
	
	public void delete(FNOrder order);
}
