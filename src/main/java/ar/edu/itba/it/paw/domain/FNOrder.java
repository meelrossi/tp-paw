package ar.edu.itba.it.paw.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class FNOrder extends PersistentEntity {
	
	private static final long serialVersionUID = 1L;

	@ManyToOne
	private User user;

	@OneToMany(cascade = CascadeType.ALL)
	private List<OrderItem> dishes;

	@Column(nullable = false)
	private double total;

	@ManyToOne
	private Restaurant restaurant;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date date;
	
	@Column(columnDefinition = "boolean default false")
	private Boolean delivered;
	
	public FNOrder() {
	}

	public FNOrder(User user, List<OrderItem> dishes,
			double total, Restaurant restaurant, Date date) {
		super();
		setUser(user);
		setDishes(dishes);
		setTotal(total);
		setRestaurant(restaurant);
		setDate(date);
		setDelivered(false);
	}

	public FNOrder(User user, List<OrderItem> dishes,
			Restaurant restaurant, Date date) {
		super();
		setUser(user);
		setDishes(dishes);
		setRestaurant(restaurant);
		setDate(date);
		calculateCost();
		setDelivered(false);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		if (user == null) {
			throw new IllegalStateException();
		}
		this.user = user;
	}

	public List<OrderItem> getDishes() {
		return dishes;
	}

	public void setDishes(List<OrderItem> dishes) {
		if (dishes == null) {
			throw new IllegalStateException();
		}
		if (dishes.isEmpty()) {
			throw new IllegalArgumentException();
		}
		this.dishes = dishes;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		if (total <= 0.0) {
			throw new IllegalArgumentException();
		}
		this.total = total;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		if (restaurant == null) {
			throw new IllegalStateException();
		}
		this.restaurant = restaurant;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		if (date == null) {
			throw new IllegalStateException();
		}
		this.date = date;
	}

	public void calculateCost() {
		for (OrderItem dish : dishes) {
			total += (dish.getDish().getPrice() * dish.getAmount());
		}
		total += restaurant.getDeliveryCost(user);
	}
	
	public void setDelivered(Boolean bool) {
		if (bool == null) {
			throw new IllegalStateException();
		}
		this.delivered = bool;
	}
	
	public Boolean getDelivered() {
		if(delivered == null) {
			delivered = false;
		}
		return delivered;
	}
}
