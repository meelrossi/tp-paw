package ar.edu.itba.it.paw.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.springframework.beans.factory.annotation.Autowired;

@Entity
public class OrderItem extends PersistentEntity {
	
	private static final long serialVersionUID = 1L;

	@ManyToOne
	private Dish dish;
	
	@Column(nullable = false)
	private int amount;
	
	@Autowired
	public OrderItem(){
	}
	
	public OrderItem(Dish dish, int amount){
		setDish(dish);
		setAmount(amount);
	}

	
	public Dish getDish() {
		return dish;
	}

	public void setDish(Dish dish) {
		if (dish == null) {
			throw new IllegalStateException();
		}
		this.dish = dish;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
}
