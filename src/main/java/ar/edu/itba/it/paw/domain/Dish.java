package ar.edu.itba.it.paw.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Dish extends PersistentEntity {
	
	private static final long serialVersionUID = 1L;
	
	public final static int DISH_NAME_MAX_SIZE = 35;
	public final static int DISH_DESCRIPTION_MAX_SIZE = 256;
	
	@Column(nullable = false, length = DISH_NAME_MAX_SIZE)
	private String name;

	@Column(nullable = true, length = DISH_DESCRIPTION_MAX_SIZE)
	private String description;

	@Column(nullable = false)
	private double price;

	@ManyToOne
	private MenuCategory menuCategory;

	public Dish() {

	}

	public Dish(String name, String description, double price) {
		super();
		setName(name);
		setDescription(description);
		setPrice(price);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null || name.length() == 0 || name.length() > DISH_NAME_MAX_SIZE) {
			throw new IllegalStateException();
		}
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if (description.length() > DISH_DESCRIPTION_MAX_SIZE) {
			throw new IllegalStateException();
		}
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		if (price <= 0){
			throw new IllegalStateException();
		}
		this.price = price;
	}

	public void setMenuCategory(MenuCategory menuCategory) {
		if (menuCategory == null) {
			throw new IllegalStateException();
		}
		this.menuCategory = menuCategory;
	}

	public MenuCategory getMenuCategory() {
		return this.menuCategory;
	}
}