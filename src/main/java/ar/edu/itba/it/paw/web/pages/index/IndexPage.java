package ar.edu.itba.it.paw.web.pages.index;

import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.repositories.RestaurantRepo;
import ar.edu.itba.it.paw.web.FoodNowSession;
import ar.edu.itba.it.paw.web.pages.BasePage;

public class IndexPage extends BasePage {
	
	private static final long serialVersionUID = 1L;
	
	@SpringBean
	private RestaurantRepo restaurantRepo;
	
	public IndexPage() {
		add(new MostPopularPanel("mostPopularPanel"));
		add(new NewNeighborhoodRestaurantsPanel("newNeighborhoodRestaurantsPanel").setVisible(FoodNowSession.get().isSignedIn()));
	}

}
