package ar.edu.itba.it.paw.web.pages.orders;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.resource.ContextRelativeResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.domain.EntityResolver;
import ar.edu.itba.it.paw.domain.FNOrder;
import ar.edu.itba.it.paw.domain.User;
import ar.edu.itba.it.paw.repositories.OrderRepo;
import ar.edu.itba.it.paw.repositories.UserRepo;
import ar.edu.itba.it.paw.web.FoodNowSession;

public class OrderPanel extends Panel {
	
	private static final long serialVersionUID = 1L;
	
	@SpringBean
	private EntityResolver entityResolver;
	
	@SpringBean
	private OrderRepo orderRepo;
	
	@SpringBean
	private UserRepo userRepo;
	
	public OrderPanel(String id) {
		super(id);
		initialize();
	}
	
	public void initialize() {
		add(new RefreshingView<FNOrder>("order") {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<FNOrder>> getItemModels() {
				List<IModel<FNOrder>> result = new ArrayList<IModel<FNOrder>>();
				User user = userRepo.get(FoodNowSession.get().getEmail());
				for (FNOrder c : orderRepo.getAll(user)) {
					final int id = c.getId();
					result.add(new LoadableDetachableModel<FNOrder>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected FNOrder load() {
							return entityResolver.fetch(FNOrder.class, id);
						}
					});
				}
				return result.iterator();
			}

			@SuppressWarnings({ "serial", "rawtypes" })
			@Override
			protected void populateItem(final Item<FNOrder> item) {
				item.add(new Label("restaurantName", item.getModelObject().getRestaurant().getName()));
				item.add(new Label("total", "Total: $" + Double.toString(item.getModelObject().getTotal())));
				SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
				item.add(new Label("creationTime", formatDate.format(item.getModelObject().getDate())));
				Image pending = new Image("pendingImg", new ContextRelativeResource("/images/delivering.png"));
				item.add(pending);
				Image delivered = new Image("deliveredImg", new ContextRelativeResource("/images/delivered.png"));
				item.add(delivered);
				Boolean isDelivered = item.getModelObject().getDelivered();
				pending.setVisible(!isDelivered);
				delivered.setVisible(isDelivered);
				Link deliveredLink = new Link("deliveredLink") {

					@Override
					public void onClick() {
						User user = userRepo.get(FoodNowSession.get().getEmail());
						List<FNOrder> orders = orderRepo.getAll(user);
						if(orders.contains(item.getModelObject())) {
							item.getModelObject().setDelivered(true);
						}
					}
				};
				item.add(deliveredLink.setVisible(!isDelivered));
				Label arrivedMessage = new Label("arrivedMessage", "El pedido fue entregado correctamente");
				item.add(arrivedMessage.setVisible(isDelivered));
			}
		});
	}
}
