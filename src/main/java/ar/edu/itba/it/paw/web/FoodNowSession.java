package ar.edu.itba.it.paw.web;

import java.util.Calendar;
import java.util.List;

import org.apache.wicket.Session;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.request.Request;

import ar.edu.itba.it.paw.domain.Action;
import ar.edu.itba.it.paw.domain.User;
import ar.edu.itba.it.paw.repositories.UserRepo;

public class FoodNowSession extends WebSession {
	
	private static final long serialVersionUID = 1L;
	
	private String email;
	private int userLvl = 5;
	private List<Action> availableActions;

	public static FoodNowSession get() {
		return (FoodNowSession) Session.get();
	}

	public FoodNowSession(Request request) {
		super(request);
	}

	public String getEmail() {
		return email;
	}

	public boolean signIn(String username, String password, UserRepo users) {
		User user = users.get(username);
		if (user != null && user.checkPassword(password)) {
			this.email = user.getEmail();
			this.userLvl = user.getUserLevel();
			user.setPreviousLogin(user.getLastLogin());
			user.setLastLogin(Calendar.getInstance());
			return true;
		}
		return false;
	}

	public boolean isSignedIn() {
		return email != null;
	}

	public void signOut() {
		invalidate();
		clear();
	}
	
	public int getUserLvl() {
		return userLvl;
	}

	public List<Action> getAvailableActions() {
		return availableActions;
	}

	public void setAvailableActions(List<Action> availableActions) {
		this.availableActions = availableActions;
	}
}