package ar.edu.itba.it.paw.web.pages.report;

import org.apache.wicket.model.LoadableDetachableModel;

public class ReportModel extends LoadableDetachableModel<Report>{
	
	private static final long serialVersionUID = 1L;
	
	private Report report;
	
	public ReportModel(Report report){
		this.report = report;
	}
	
	@Override
	protected Report load() {
		return report;
	}

}
