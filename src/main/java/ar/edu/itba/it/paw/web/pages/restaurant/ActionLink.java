package ar.edu.itba.it.paw.web.pages.restaurant;

import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.domain.FNOrder;
import ar.edu.itba.it.paw.domain.User;
import ar.edu.itba.it.paw.repositories.OrderRepo;
import ar.edu.itba.it.paw.repositories.RestaurantRepo;
import ar.edu.itba.it.paw.repositories.UserRepo;
import ar.edu.itba.it.paw.web.pages.restaurant.modifyRestaurant.ModifyRestaurantPage;

public class ActionLink extends Link<Void> {

	private static final long serialVersionUID = 1L;
	
	@SpringBean
	private RestaurantRepo restaurantRepo;

	@SpringBean
	private OrderRepo orderRepo;
	
	@SpringBean
	private UserRepo userRepo;
	
	private String actionName;
	private int restId;
	
	public ActionLink(String id, String actionName, int restId) {
		super(id);
		this.setActionName(actionName);
		this.restId = restId;
	}

	@Override
	public void onClick() {
		if(actionName.equals("remove")){
			for(FNOrder order: restaurantRepo.get(restId).getOrders()){
				User user = userRepo.get(order.getUser().getId());
				user.getOrders().remove(order);
				userRepo.add(user);
				restaurantRepo.get(restId).getOrders().remove(order);
				orderRepo.delete(order);
			}
			restaurantRepo.delete(restId);
			setResponsePage(new RestaurantListPage());
		} else if(actionName.equals("assignManager")){
			setResponsePage(new AssignManagerPage(restId));
		} else if(actionName.equals("edit")) {
			setResponsePage(new ModifyRestaurantPage(restId));
		} else {
			
		}
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

}
