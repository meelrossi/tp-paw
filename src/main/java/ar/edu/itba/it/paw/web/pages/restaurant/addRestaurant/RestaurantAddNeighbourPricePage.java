package ar.edu.itba.it.paw.web.pages.restaurant.addRestaurant;

import ar.edu.itba.it.paw.domain.EntityModel;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.web.pages.BasePage;
import ar.edu.itba.it.paw.web.pages.restaurant.MenuPanel;

public class RestaurantAddNeighbourPricePage extends BasePage {

	public RestaurantAddNeighbourPricePage(Restaurant restaurant) {
		add(new NeighbourhoodPricePanel("neighborhoodPricePanel", new EntityModel<Restaurant>(Restaurant.class, restaurant)));
	}

}
