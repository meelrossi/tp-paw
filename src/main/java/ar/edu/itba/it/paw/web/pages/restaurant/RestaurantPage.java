package ar.edu.itba.it.paw.web.pages.restaurant;

import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import ar.edu.itba.it.paw.domain.ClosingTerm;
import ar.edu.itba.it.paw.domain.EntityModel;
import ar.edu.itba.it.paw.domain.EntityResolver;
import ar.edu.itba.it.paw.domain.Rating;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.domain.User;
import ar.edu.itba.it.paw.repositories.RatingRepo;
import ar.edu.itba.it.paw.repositories.RestaurantRepo;
import ar.edu.itba.it.paw.repositories.UserRepo;
import ar.edu.itba.it.paw.web.FoodNowSession;
import ar.edu.itba.it.paw.web.pages.BasePage;
import ar.edu.itba.it.paw.web.pages.restaurant.addDish.AddDishPage;

@SuppressWarnings("deprecation")
public class RestaurantPage extends BasePage {
	
	private static final long serialVersionUID = 1L;

	@SpringBean
	private RestaurantRepo restaurantRepo;
	
	@SpringBean
	private UserRepo userRepo;
	
	@SpringBean
	private RatingRepo ratingRepo;
	
	@SpringBean
	private EntityResolver entityResolver;
	

	private transient String description;
	private transient Double value;
	
	@SuppressWarnings({ "rawtypes", "serial" })
	public RestaurantPage(int restaurantId) {
		final Restaurant restaurant = restaurantRepo.get(restaurantId);
		final User user = userRepo.get(FoodNowSession.get().getEmail());
		add(new Label("restaurantName", restaurant.getName()));
		add(new Label("address", restaurant.getAddress().getStreet() + " " + String.valueOf(restaurant.getAddress().getNumber())));
		add(new Label("deliveryTime", restaurant.getDeliveryTime().getDescription()));
		add(new Label("deliveryCost", restaurant.getDeliveryCostString(user)));
		add(new Label("description", restaurant.getDescription()));
		add(new Label("minAmount", "$" +  String.valueOf(restaurant.getMinAmount())));
		add(new Label("nextDeliveryMessage", new StringResourceModel("deliveryTimeMessage", this, new Model<Restaurant>(restaurant))).setVisible(restaurant.canHaveDeliveries()));
		add(new MenuPanel("menuPanel",new EntityModel<Restaurant>(Restaurant.class, restaurant)));
		ClosingTerm closingTerm = restaurant.getClosingTerm();
		String message = "";
		SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
		if(closingTerm != null) {
			message = closingTerm.getMotive() + " " + formatDate.format(closingTerm.getStartDate().getTime()) + " - " + formatDate.format(closingTerm.getEndDate().getTime());
		}
		add(new Label("closeMessage", message).setVisible( closingTerm != null));
		add(new Fragment ("contentArea", "nextDeliveryFragment", this).setVisible(false));
		add(new NumberTextField("rating")
				.add(new SimpleAttributeModifier("value", String
						.valueOf(restaurant.getRating()))));
		
		Link createOrderLink = new Link("createOrderLink") {
			@Override
			public void onClick() {
				
			}
		};
		add(createOrderLink.setVisible(restaurant.canMakeOrder(user) && closingTerm == null));
		
		Link addDishLink = new Link("addDishLink") {
			@Override
			public void onClick() {
				setResponsePage(new AddDishPage(restaurant.getId()));
			}
		};
		add(addDishLink.setVisible(restaurant.canAddDish(user)));
		
		Link calificationsLink = new Link("calificationsLink") {
			@Override
			public void onClick() {
				setResponsePage(new RatingPage(restaurant));
			}
		};
		add(calificationsLink);
		
		Button rateLink = new Button("rateLink");
		add(rateLink.setVisible(restaurant.canRate(user)));
		
		FoodNowSession s = FoodNowSession.get();
		List<String> managers = restaurant.getAllManagerMails();
		Link addClosingTermLink = new Link("addClosingTermLink") {
			@Override
			public void onClick() {
				setResponsePage(new ClosingTermPage(restaurant));
			}
		};
		add(addClosingTermLink.setVisible(managers.contains(s.getEmail())));
		
		Form<RestaurantPage> ratingForm = new Form<RestaurantPage>("ratingForm",
				new CompoundPropertyModel<RestaurantPage>(this)) {
					private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit() {
				Rating rat = new Rating(user, restaurant, description, value);
				ratingRepo.add(rat);
				restaurant.addRating(rat);
				setResponsePage(new RestaurantPage(restaurant.getId()));
			}
		};
		ratingForm.add(new TextArea<String>("description").add(
				new MaximumLengthValidator(Rating.RATING_DESCRIPTION_MAX_SIZE))
				.setRequired(true));
		ratingForm.add(new NumberTextField("value").setRequired(true));
		add(ratingForm);
	}
}
