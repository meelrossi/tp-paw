package ar.edu.itba.it.paw.web.pages.restaurant;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.domain.Dish;
import ar.edu.itba.it.paw.domain.EntityResolver;
import ar.edu.itba.it.paw.domain.MenuCategory;

public class DishPanel extends Panel {
	private static final long serialVersionUID = 1L;

	@SpringBean
	private EntityResolver entityResolver;
	
	private transient Map<String, String> ordersMap;

	public DishPanel(String id, IModel<MenuCategory> model, Map<String, String> ordersMap) {
		super(id);
		initialize(model);
		this.ordersMap = ordersMap;
	}

	private void initialize(final IModel<MenuCategory> model) {
		add(new RefreshingView<Dish>("dish") {
			private static final long serialVersionUID = 1L;
			private Component add;

			@Override
			protected Iterator<IModel<Dish>> getItemModels() {
				List<IModel<Dish>> result = new ArrayList<IModel<Dish>>();
				for (Dish c : model.getObject().getDishes()) {
					final int id = c.getId();
					result.add(new LoadableDetachableModel<Dish>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected Dish load() {
							return entityResolver.fetch(Dish.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<Dish> item) {
				item.add(new Label("name", item.getModelObject().getName()));
				item.add(new Label("description", item.getModelObject().getDescription()));
				item.add(new Label("price", "$" + item.getModelObject().getPrice()));
				NumberTextField<Integer> dishValue = (NumberTextField<Integer>) new NumberTextField<Integer>("value", new PropertyModel<Integer>(ordersMap,Integer.toString(item.getModelObject().getId()))).add(new SimpleAttributeModifier("min", String
						.valueOf(0))).add(new SimpleAttributeModifier("max", String
						.valueOf(5)));
				item.add(dishValue);
			}
		});
	}
}
