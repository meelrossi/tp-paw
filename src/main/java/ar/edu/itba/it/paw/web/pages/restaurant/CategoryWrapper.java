package ar.edu.itba.it.paw.web.pages.restaurant;

import java.io.Serializable;

public class CategoryWrapper implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String name;
    private Boolean selected = Boolean.FALSE;
    
    public CategoryWrapper(String wrapped)
    {
        this.name = wrapped;
    }
 
    public Boolean getSelected()
    {
        return selected;
    }
 
    public void setSelected(Boolean selected)
    {
        this.selected = selected;
    }
 
    public String getName()
    {
        return name;
    }
 
    public void setName(String wrapped)
    {
        this.name = wrapped;
    }
     
    public String toString()
    {
        return name + ": " + selected;
    }

}
