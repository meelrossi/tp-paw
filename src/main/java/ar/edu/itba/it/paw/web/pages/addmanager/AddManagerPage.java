package ar.edu.itba.it.paw.web.pages.addmanager;

import java.util.Calendar;
import java.util.List;

import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.EmailTextField;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.validation.EqualInputValidator;
import org.apache.wicket.markup.html.form.validation.EqualPasswordInputValidator;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;
import org.apache.wicket.validation.validator.StringValidator.MinimumLengthValidator;

import ar.edu.itba.it.paw.domain.Address;
import ar.edu.itba.it.paw.domain.Neighborhood;
import ar.edu.itba.it.paw.domain.User;
import ar.edu.itba.it.paw.repositories.NeighborhoodRepo;
import ar.edu.itba.it.paw.repositories.UserRepo;
import ar.edu.itba.it.paw.web.pages.BasePage;
import ar.edu.itba.it.paw.web.pages.login.FoodNowFeedbackPanel;
import ar.edu.itba.it.paw.web.pages.login.LoginPage;

@SuppressWarnings({ "serial" , "unused" })
public class AddManagerPage extends BasePage {

	@SpringBean
	private UserRepo userRepo;

	@SpringBean
	private NeighborhoodRepo neighborhoodRepo;
	
	private transient String email;
	private transient String password;
	private transient String name;
	private transient String lastName;
	private transient Address address;
	private transient Calendar birthDate;

	private transient String regemail;
	private transient String regpassword;
	private transient String reEmail;
	private transient String rePassword;
	private transient String addressStreet;
	private transient Integer addressNumber;
	private transient List<String> neighborhoods;
	private transient String neighborhood;
	
	private transient List<String> usersEmails;
	private transient String userEmail;

	public AddManagerPage() {
		add(new FoodNowFeedbackPanel("feedback"));
		
		Form<AddManagerPage> registerForm = new Form<AddManagerPage>("registerForm",
				new CompoundPropertyModel<AddManagerPage>(this)) {
			@Override
			protected void onSubmit() {

				User user = userRepo.get(regemail);

				if (user != null) {
					error(new StringResourceModel("userAlreadyExist", this,
							null).getString());
					return;
				}

				Neighborhood hood = neighborhoodRepo.get(neighborhood);

				address = new Address(addressStreet, addressNumber, hood);

				user = new User(name, lastName, address, regemail, regpassword,
						birthDate, 1);
				userRepo.add(user);

				success(new StringResourceModel("creationSuccess", this, null)
						.getString());

			}
		};

		PasswordTextField pass = new PasswordTextField("regpassword");
		PasswordTextField repass = new PasswordTextField("rePassword");
		EmailTextField email = new EmailTextField("regemail");
		EmailTextField reemail = new EmailTextField("reEmail");

		registerForm.add(email.add(
				new MaximumLengthValidator(User.EMAIL_MAX_SIZE)).setRequired(
				true));
		registerForm.add(reemail.add(
				new MaximumLengthValidator(User.EMAIL_MAX_SIZE)).setRequired(
				true));
		registerForm.add(new EqualInputValidator(email, reemail));

		registerForm.add(pass
				.add(new MaximumLengthValidator(User.PASSWORD_MAX_SIZE))
				.add(new MinimumLengthValidator(6)).setRequired(true));
		registerForm.add(repass
				.add(new MaximumLengthValidator(User.PASSWORD_MAX_SIZE))
				.add(new MinimumLengthValidator(6)).setRequired(true));
		registerForm.add(new EqualPasswordInputValidator(pass, repass));

		registerForm.add(new TextField<String>("name").add(
				new MaximumLengthValidator(User.FIRST_NAME_MAX_SIZE))
				.setRequired(true));
		registerForm.add(new TextField<String>("lastName").add(
				new MaximumLengthValidator(User.LAST_NAME_MAX_SIZE))
				.setRequired(true));
		registerForm.add(new TextField<String>("addressStreet")
				.setRequired(true));
		registerForm.add(new TextField<Number>("addressNumber")
				.setRequired(true));
		registerForm.add(new DateTextField("birthDate").setRequired(true));

		neighborhoods = neighborhoodRepo.getAllNames();

		DropDownChoice<String> ddc = new DropDownChoice<String>("neighborhood",
				new PropertyModel<String>(this, "neighborhood"), neighborhoods);

		registerForm.add(ddc.setRequired(true));
		registerForm.add(new Button("regsubmit", new ResourceModel("submit")));

		add(registerForm);
		
		
		Form<AddManagerPage> updateToManagerForm = new Form<AddManagerPage>("updateToManagerForm",
				new CompoundPropertyModel<AddManagerPage>(this)) {
			@Override
			protected void onSubmit() {

				User user = userRepo.get(userEmail);

				user.setUserLevel(1);
				
				userRepo.add(user);

				success(new StringResourceModel("creationSuccess", this, null)
						.getString());

			}
		};
		
		
		usersEmails = userRepo.getAllUsersMails();
		
		DropDownChoice<String> ddcUsers = new DropDownChoice<String>("userEmail",
				new PropertyModel<String>(this, "userEmail"), usersEmails);

		updateToManagerForm.add(ddcUsers.setRequired(true));
		updateToManagerForm.add(new Button("updateSubmit", new ResourceModel("submit")));
		
		add(updateToManagerForm);
		
	}

}
