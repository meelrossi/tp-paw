package ar.edu.itba.it.paw.web.pages.login;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.EmailTextField;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.validation.EqualInputValidator;
import org.apache.wicket.markup.html.form.validation.EqualPasswordInputValidator;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;
import org.apache.wicket.validation.validator.StringValidator.MinimumLengthValidator;

import ar.edu.itba.it.paw.domain.Address;
import ar.edu.itba.it.paw.domain.Neighborhood;
import ar.edu.itba.it.paw.domain.User;
import ar.edu.itba.it.paw.repositories.ChangePasswordPropertyRepo;
import ar.edu.itba.it.paw.repositories.NeighborhoodRepo;
import ar.edu.itba.it.paw.repositories.UserRepo;
import ar.edu.itba.it.paw.web.FoodNowSession;
import ar.edu.itba.it.paw.web.pages.BasePage;
import ar.edu.itba.it.paw.web.pages.index.IndexPage;

@SuppressWarnings({ "serial", "unused"})
public class LoginPage extends BasePage {

	private static final long serialVersionUID = 1L;

	@SpringBean
	private UserRepo userRepo;

	@SpringBean
	private NeighborhoodRepo neighborhoodRepo;
	
	@SpringBean
	private ChangePasswordPropertyRepo changePasswordPropertyRepo;

	private transient String email;
	private transient String password;
	private transient String name;
	private transient String lastName;
	private transient Address address;
	private transient Calendar birthDate;

	private transient String regemail;
	private transient String regpassword;
	private transient String reEmail;
	private transient String rePassword;
	private transient String addressStreet;
	private transient Integer addressNumber;
	private transient List<String> neighborhoods;
	private transient String neighborhood;

	public LoginPage() {
		add(new FoodNowFeedbackPanel("feedback"));
		Form<LoginPage> loginForm = new Form<LoginPage>("loginForm",
				new CompoundPropertyModel<LoginPage>(this)) {
			@Override
			protected void onSubmit() {
				FoodNowSession s = FoodNowSession.get();
				User user = userRepo.get(email);
				if (s.signIn(email, password, userRepo)) {
					if (!continueToOriginalDestination()) {
							Calendar expirationDate = user.getLastPasswordChange();
							expirationDate.add(Calendar.DATE, changePasswordPropertyRepo.getPasswordPropery());
							Calendar today = Calendar.getInstance();
						if (expirationDate.before(today)) {
							setResponsePage(new ChangePasswordPage());							
						} else {
							setResponsePage(IndexPage.class);							
						}
					}
				} else {
					error(new StringResourceModel("wrongPassOrUser", this, null)
							.getString());
				}
			}
		};
		loginForm.add(new EmailTextField("email").add(
				new MaximumLengthValidator(User.EMAIL_MAX_SIZE)).setRequired(
				true));
		loginForm.add(new PasswordTextField("password").add(
				new MaximumLengthValidator(User.PASSWORD_MAX_SIZE))
				.setRequired(true));
		loginForm.add(new Button("submit", new ResourceModel("submit")));
		add(loginForm);

		Form<LoginPage> registerForm = new Form<LoginPage>("registerForm",
				new CompoundPropertyModel<LoginPage>(this)) {
			@Override
			protected void onSubmit() {

				User user = userRepo.get(regemail);

				if (user != null) {
					error(new StringResourceModel("userAlreadyExist", this,
							null).getString());
					return;
				}

				Neighborhood hood = neighborhoodRepo.get(neighborhood);

				address = new Address(addressStreet, addressNumber, hood);

				user = new User(name, lastName, address, regemail, regpassword,
						birthDate, 2);
				userRepo.add(user);

				success(new StringResourceModel("creationSuccess", this, null)
						.getString());

			}
		};

		PasswordTextField pass = new PasswordTextField("regpassword");
		PasswordTextField repass = new PasswordTextField("rePassword");
		EmailTextField email = new EmailTextField("regemail");
		EmailTextField reemail = new EmailTextField("reEmail");

		registerForm.add(email.add(
				new MaximumLengthValidator(User.EMAIL_MAX_SIZE)).setRequired(
				true));
		registerForm.add(reemail.add(
				new MaximumLengthValidator(User.EMAIL_MAX_SIZE)).setRequired(
				true));
		registerForm.add(new EqualInputValidator(email, reemail));

		registerForm.add(pass
				.add(new MaximumLengthValidator(User.PASSWORD_MAX_SIZE))
				.add(new MinimumLengthValidator(6)).setRequired(true));
		registerForm.add(repass
				.add(new MaximumLengthValidator(User.PASSWORD_MAX_SIZE))
				.add(new MinimumLengthValidator(6)).setRequired(true));
		registerForm.add(new EqualPasswordInputValidator(pass, repass));

		registerForm.add(new TextField<String>("name").add(
				new MaximumLengthValidator(User.FIRST_NAME_MAX_SIZE))
				.setRequired(true));
		registerForm.add(new TextField<String>("lastName").add(
				new MaximumLengthValidator(User.LAST_NAME_MAX_SIZE))
				.setRequired(true));
		registerForm.add(new TextField<String>("addressStreet")
				.setRequired(true));
		registerForm.add(new TextField<Number>("addressNumber")
				.setRequired(true));
		registerForm.add(new DateTextField("birthDate").setRequired(true));

		neighborhoods = neighborhoodRepo.getAllNames();

		DropDownChoice<String> ddc = new DropDownChoice<String>("neighborhood",
				new PropertyModel<String>(this, "neighborhood"), neighborhoods);

		registerForm.add(ddc.setRequired(true));
		registerForm.add(new Button("regsubmit", new ResourceModel("submit")));

		add(registerForm);

	};
}
