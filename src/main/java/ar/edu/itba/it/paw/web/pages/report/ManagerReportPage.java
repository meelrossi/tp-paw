package ar.edu.itba.it.paw.web.pages.report;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.itba.it.paw.domain.FNOrder;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.domain.User;
import ar.edu.itba.it.paw.repositories.RestaurantRepo;
import ar.edu.itba.it.paw.repositories.UserRepo;
import ar.edu.itba.it.paw.web.FoodNowSession;
import ar.edu.itba.it.paw.web.pages.BasePage;
import ar.edu.itba.it.paw.web.pages.login.FoodNowFeedbackPanel;

@SuppressWarnings("serial")
public class ManagerReportPage extends BasePage {

	@SpringBean
	private RestaurantRepo restaurantRepo;

	@SpringBean
	private UserRepo userRepo;

	private transient Calendar startDate;
	private transient Calendar endDate;

	public ManagerReportPage() {

		add(new FoodNowFeedbackPanel("feedback"));
		Form<ManagerReportPage> reportForm = new Form<ManagerReportPage>(
				"reportForm",
				new CompoundPropertyModel<ManagerReportPage>(this)) {

			@Override
			protected void onSubmit() {
				super.onSubmit();
			}
		};

		reportForm.add(new DateTextField("startDate").setRequired(true));
		reportForm.add(new DateTextField("endDate").setRequired(true));
		reportForm.add(new Button("reportSubmit", new ResourceModel("submit")));

		add(reportForm);

		add(new RefreshingView<Report>("report") {
			private static final long serialVersionUID = 1L;

			@SuppressWarnings({ "unused", "deprecation" })
			@Override
			protected Iterator<IModel<Report>> getItemModels() {
				List<IModel<Report>> result = new ArrayList<IModel<Report>>();

				User user = userRepo.get(FoodNowSession.get().getEmail());

				Map<String, Report> reportMap = new HashMap<String, Report>();

				if (endDate != null && startDate != null) {
					for (Restaurant rest : user.getRestaurants()) {
						for (FNOrder order : rest.getOrders()) {
						
							if (order.getDate().before(endDate.getTime())
									&& order.getDate().after(
											startDate.getTime())) {
								String key = order.getDate().getDay() + "/"
										+ order.getDate().getMonth() + "/"
										+ order.getDate().getYear() + "-"
										+ rest.getId();
								if (reportMap.containsKey(key)) {
									reportMap
											.get(key)
											.setCantOrders(
													reportMap.get(key)
															.getCantOrders() + 1);
								} else {
									Calendar date = Calendar.getInstance();
									date.setTime(order.getDate());
									Report report = new Report(date,
											rest.getName(), 1, rest
													.getAddress()
													.getNeighborhood()
													.getName(), rest.getId());
									reportMap.put(key, report);
								}

							}
						}
					}
				}
				
				for (Report r : reportMap.values()) {
					result.add(new ReportModel(r));
				}
				sortModels(result);

				return result.iterator();
			}

			@Override
			protected void populateItem(Item<Report> item) {
				Report report = item.getModelObject();

				item.add(new ReportDetailLink("reportDetailLink", report
						.getRestaurantId(), report.getHood(), report.getDate()));

				item.add(new Label("restaurant", report.getRestaurantName()));
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

				item.add(new Label("date", df
						.format(report.getDate().getTime())));

				item.add(new Label("cant", String.valueOf(report
						.getCantOrders())));
				item.add(new Label("hood", report.getHood()));

			}
		});

		add(new WebMarkupContainer("table") {
			@Override
			public boolean isVisible() {
				return startDate != null;
			}
		});

	}
	
	private void sortModels(List<IModel<Report>> orders) {
		Collections.sort(orders, new Comparator<IModel<Report>>() {

	        public int compare(IModel<Report> o1, IModel<Report> o2) {

	            Calendar x1 = o1.getObject().getDate();
	            Calendar x2 = o2.getObject().getDate();
	            int sComp = x1.compareTo(x2);

	            if (sComp != 0) {
	               return sComp;
	            } else {
	               String y1 = o1.getObject().getHood();
	               String y2 = o2.getObject().getHood();
	               return y1.compareTo(y2);
	            }
	        }
		});
	}
}
