package ar.edu.itba.it.paw.repositories;

import ar.edu.itba.it.paw.domain.OrderItem;

public interface OrderItemRepo {
	
	public void add(OrderItem orderItem);
}
