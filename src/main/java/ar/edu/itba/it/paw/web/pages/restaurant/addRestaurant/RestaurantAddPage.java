package ar.edu.itba.it.paw.web.pages.restaurant.addRestaurant;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;
import org.joda.time.LocalTime;

import ar.edu.itba.it.paw.domain.Address;
import ar.edu.itba.it.paw.domain.DeliveryNeighborhood;
import ar.edu.itba.it.paw.domain.DeliveryTime;
import ar.edu.itba.it.paw.domain.Neighborhood;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.repositories.NeighborhoodRepo;
import ar.edu.itba.it.paw.repositories.RestaurantRepo;
import ar.edu.itba.it.paw.repositories.UserRepo;
import ar.edu.itba.it.paw.web.pages.BasePage;
import ar.edu.itba.it.paw.web.pages.login.FoodNowFeedbackPanel;
import ar.edu.itba.it.paw.web.pages.restaurant.DeliveryNeighborhoodWrapper;

public class RestaurantAddPage extends BasePage {

	private static final long serialVersionUID = 1L;

	@SpringBean
	private UserRepo userRepo;

	@SpringBean
	private NeighborhoodRepo neighborhoodRepo;

	@SpringBean
	private RestaurantRepo restaurantRepo;

	private transient Address address;
	private transient String restName;
	private transient String addressStreet;
	private transient Integer addressNumber;
	private transient String neighborhood;
	private transient Double minAmount;
	private transient String description;
	private transient List<String> neighborhoods;
	private transient String openingTime;
	private transient String closingTime;

	ArrayList<String> selectedNeighborhood = new ArrayList<String>();
	List<DeliveryNeighborhoodWrapper> neighbors;
	private String[] selectedNeighborhoods;

	public RestaurantAddPage() {
		add(new FoodNowFeedbackPanel("feedback"));

		Form<RestaurantAddPage> addRestaurantForm = new Form<RestaurantAddPage>("addRestaurantForm",
				new CompoundPropertyModel<RestaurantAddPage>(this)) {

			@Override
			protected void onSubmit() {

				if (restaurantRepo.exists(restName)) {
					error(new StringResourceModel("restaurantAlreadyExist", this, null).getString());
					return;
				}

				Neighborhood hood = neighborhoodRepo.get(neighborhood);

				address = new Address(addressStreet, addressNumber, hood);

				DeliveryTime deliveryTime;
				try {
					validateTime(openingTime);
					validateTime(closingTime);
				} catch (Exception e) {
					error(new StringResourceModel("wrongTime", this, null).getString());
					return;
				}
				deliveryTime = new DeliveryTime(openingTime, closingTime);

				Restaurant restaurant = new Restaurant(restName, description != null ? description : "", minAmount,
						address, deliveryTime);

				List<DeliveryNeighborhood> selectedNeighborhoodList = new ArrayList<DeliveryNeighborhood>();
				for (DeliveryNeighborhoodWrapper n : neighbors) {
					if (n.getSelected())
						selectedNeighborhoodList.add(n.getDeliveryNeighborhood());
				}

				if (selectedNeighborhoodList.isEmpty()) {
					error(new StringResourceModel("noNeighborhoodSelected", this, null).getString());
					return;
				}

				restaurant.setDeliveries(selectedNeighborhoodList);
				restaurantRepo.add(restaurant);

				success(new StringResourceModel("creationSuccess", this, null).getString());
				setResponsePage(new RestaurantAddNeighbourPricePage(restaurant));
			}
		};

		addRestaurantForm.add(new TextField<String>("restName")
				.add(new MaximumLengthValidator(Restaurant.RESTAURANT_NAME_MAX_SIZE)).setRequired(true));
		addRestaurantForm.add(new TextField<String>("description")
				.add(new MaximumLengthValidator(Restaurant.RESTAURANT_DESCRIPTION_MAX_SIZE)));
		addRestaurantForm.add(new TextField<String>("addressStreet").setRequired(true));
		addRestaurantForm.add(new TextField<Number>("addressNumber").setRequired(true));
		addRestaurantForm.add(new TextField<Double>("minAmount").setRequired(true));
		addRestaurantForm.add(new TextField<String>("openingTime").setRequired(true));
		addRestaurantForm.add(new TextField<String>("closingTime").setRequired(true));
		neighborhoods = neighborhoodRepo.getAllNames();
		DropDownChoice<String> ddc = new DropDownChoice<String>("neighborhood",
				new PropertyModel<String>(this, "neighborhood"), neighborhoods);
		addRestaurantForm.add(ddc.setRequired(true));

		neighbors = new ArrayList<DeliveryNeighborhoodWrapper>();
		for (Neighborhood n : neighborhoodRepo.getAll()) {
			neighbors.add(new DeliveryNeighborhoodWrapper(n));
		}

		ListView listView = new ListView("list", neighbors) {
			@Override
			protected void populateItem(ListItem item) {
				DeliveryNeighborhoodWrapper wrapper = (DeliveryNeighborhoodWrapper) item.getModelObject();
				item.add(new Label("name", wrapper.getName()));
				item.add(new CheckBox("check", new PropertyModel(wrapper, "selected")));
			}
		};
		listView.setReuseItems(true);
		addRestaurantForm.add(listView);

		addRestaurantForm.add(new Button("regsubmit", new ResourceModel("submit")));

		add(addRestaurantForm);
	}

	public boolean validateTime(String time) {
		return LocalTime.parse(time) != null;
	}

}
