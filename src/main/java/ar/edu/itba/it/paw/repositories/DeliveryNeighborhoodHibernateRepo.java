package ar.edu.itba.it.paw.repositories;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.edu.itba.it.paw.domain.DeliveryNeighborhood;

@Repository
public class DeliveryNeighborhoodHibernateRepo extends AbstractHibernateRepo implements DeliveryNeighborhoodRepo {
	
	@Autowired
	public DeliveryNeighborhoodHibernateRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public DeliveryNeighborhood getNeighborhood(int id) {
		return (DeliveryNeighborhood) find("from DeliveryNeighborhood where id = ?", id).get(0);
	}

	public void add(DeliveryNeighborhood n) {
		save(n);
	}

}
