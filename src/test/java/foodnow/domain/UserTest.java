package foodnow.domain;

import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ar.edu.itba.it.paw.domain.Address;
import ar.edu.itba.it.paw.domain.DeliveryTime;
import ar.edu.itba.it.paw.domain.Dish;
import ar.edu.itba.it.paw.domain.FNOrder;
import ar.edu.itba.it.paw.domain.Neighborhood;
import ar.edu.itba.it.paw.domain.OrderItem;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.domain.User;

public class UserTest {
	
	private User user;
	private Restaurant restaurant;
	
	@Before
	public void setup(){
		Neighborhood neighborhood = new Neighborhood("neighborhood");
		Address address = new Address("street", 123, neighborhood);
		user = new User("firstname", "lastname", address, "email@paw.com", "password", Calendar.getInstance(), 2);
		Neighborhood restneighborhood = new Neighborhood("neighborhood2");
		Address restaddress = new Address("street2", 456, restneighborhood);
		DeliveryTime deliveryTime = new DeliveryTime("18:00", "23:59");
		restaurant = new Restaurant("name", "This is a restaurant", 30, restaddress, deliveryTime);
	}
	
	@Test
	public void validUserTest(){
		String firstname = "firstname";
		String lastname = "lastname";
		Neighborhood neighborhood = new Neighborhood("neighborhood");
		Address address = new Address("street", 123, neighborhood);
		String email = "email@paw.com";
		String password = "password";
		Calendar calendar = Calendar.getInstance();
		int userLevel = 2;
		new User(firstname, lastname, address, email, password, calendar, userLevel);
		}
	
	@Test(expected = IllegalStateException.class)
	public void tooLongFirstNameTest(){
		user.setName("too long firstnameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
	}
	
	@Test(expected = IllegalStateException.class)
	public void tooLongLastNameTest(){
		user.setLastname("too long lastnameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
	}
	
	@Test(expected = IllegalStateException.class)
	public void tooLongEmailTest(){
		user.setEmail("email@paw.commmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
	}
	
	@Test(expected = IllegalStateException.class)
	public void invalidEmailTest(){
		user.setEmail("email");
	}
	
	@Test(expected = IllegalStateException.class)
	public void tooLongPasswordTest(){
		user.setPassword("passworddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
	}
	
	@Test(expected = IllegalStateException.class)
	public void emptyFirstNameTest(){
		user.setName("");
	}
	
	@Test(expected = IllegalStateException.class)
	public void emptyLastNameTest(){
		user.setLastname("");
	}
	
	@Test(expected = IllegalStateException.class)
	public void emptyEmailTest(){
		user.setEmail("");
	}
	
	@Test(expected = IllegalStateException.class)
	public void emptyPasswordTest(){
		user.setPassword("");
	}
	
	@Test
	public void addOrderTest(){
		Dish dish = new Dish("dish", "dish description", 30);
		OrderItem orderItem = new OrderItem(dish, 3);
		List<OrderItem> dishes = new LinkedList<OrderItem>();
		dishes.add(orderItem);
		FNOrder order = new FNOrder(user, dishes, restaurant, new Date());
		user.addOrder(order);
		assertTrue(!user.getOrders().isEmpty());
	}
	
	@Test(expected = IllegalStateException.class)
	public void addRestaurantCommonUser(){
		user.addRestaurant(restaurant);
	}
	
	@Test
	public void addRestaurantManagerUser(){
		user.setUserLevel(1);
		assertTrue(user.getRestaurants().isEmpty());
		user.addRestaurant(restaurant);
		assertTrue(!user.getRestaurants().isEmpty());
	}

}
