package foodnow.domain;

import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ar.edu.itba.it.paw.domain.Address;
import ar.edu.itba.it.paw.domain.DeliveryTime;
import ar.edu.itba.it.paw.domain.Dish;
import ar.edu.itba.it.paw.domain.MenuCategory;
import ar.edu.itba.it.paw.domain.Neighborhood;
import ar.edu.itba.it.paw.domain.Rating;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.domain.User;

public class RestaurantTest {

	Restaurant restaurant;
	User manager;

	@Before
	public void setup() {
		Neighborhood restneighborhood = new Neighborhood("neighborhood2");
		Address restaddress = new Address("street2", 456, restneighborhood);
		DeliveryTime deliveryTime = new DeliveryTime("09:10", "23:59");
		restaurant = new Restaurant("name", "This is a restaurant", 30,
				restaddress, deliveryTime);
		String firstname = "firstname";
		String lastname = "lastname";
		Neighborhood neighborhood = new Neighborhood("neighborhood");
		Address address = new Address("street", 123, neighborhood);
		String email = "email@paw.com";
		String password = "password";
		Calendar calendar = Calendar.getInstance();
		int userLevel = 1;
		manager = new User(firstname, lastname, address, email, password,
				calendar, userLevel);
	}

	@Test(expected = IllegalStateException.class)
	public void tooLongRestaurantNameTest() {
		restaurant
				.setName("too long Restaurant nameeeeeeeeeeeeeeeeeeeeeeeeeee");
	}

	@Test(expected = IllegalStateException.class)
	public void emptyRestaurantNameTest() {
		restaurant.setName("");
	}

	@Test(expected = IllegalStateException.class)
	public void tooLongRestaurantDescriptionTest() {
		restaurant
				.setDescription("too long Restaurant description; too long Restaurant description; too long Restaurant description; too long Restaurant description; too long Restaurant description; too long Restaurant description; too long Restaurant description; too long Restaurant description; too long Restaurant description; too long Restaurant description; too long Restaurant description; ");
	}

	@Test(expected = IllegalArgumentException.class)
	public void invalidMinAmountTest() {
		restaurant.setMinAmount(-20);
	}

	@Test
	public void addManagerTest() {
		assertTrue(restaurant.getManagers().isEmpty());
		restaurant.addManager(manager);
		assertTrue(!restaurant.getManagers().isEmpty());
	}

	@Test
	public void addRatingTest() {
		String firstname = "firstname";
		String lastname = "lastname";
		Neighborhood neighborhood = new Neighborhood("neighborhood");
		Address address = new Address("street", 123, neighborhood);
		String email = "email@paw.com";
		String password = "password";
		Calendar calendar = Calendar.getInstance();
		int userLevel = 2;
		User user1 = new User(firstname, lastname, address, email, password,
				calendar, userLevel);
		User user2 = new User(firstname + "2", lastname + "2", address, email,
				password + "2", calendar, userLevel);
		Rating rating1 = new Rating(user1, restaurant, "rating description", 3);
		Rating rating2 = new Rating(user2, restaurant, "rating description", 5);
		assertTrue(restaurant.getAllRatings().isEmpty());
		restaurant.addRating(rating1);
		restaurant.addRating(rating2);
		assertTrue(!restaurant.getAllRatings().isEmpty());
		assertTrue(restaurant.getRating() == 4);
	}

	@Test
	public void canRateTest() {
		String firstname = "firstname";
		String lastname = "lastname";
		Neighborhood neighborhood = new Neighborhood("neighborhood");
		Address address = new Address("street", 123, neighborhood);
		String email = "email@paw.com";
		String password = "password";
		Calendar calendar = Calendar.getInstance();
		int userLevel1 = 1;
		int userLevel2 = 2;
		User user1 = new User(firstname, lastname, address, email, password,
				calendar, userLevel1);
		User user2 = new User(firstname + "2", lastname + "2", address, email,
				password + "2", calendar, userLevel2);
		Rating rating1 = new Rating(user1, restaurant, "rating description", 3);
		restaurant.addRating(rating1);
		assertTrue(!restaurant.canRate(user1));
		assertTrue(restaurant.canRate(user2));
	}

	@Test
	public void canAddDishTest() {
		String firstname = "firstname";
		String lastname = "lastname";
		Neighborhood neighborhood = new Neighborhood("neighborhood");
		Address address = new Address("street", 123, neighborhood);
		String email = "email@paw.com";
		String password = "password";
		Calendar calendar = Calendar.getInstance();
		int userLevel1 = 1;
		int userLevel2 = 2;
		User user1 = new User(firstname, lastname, address, email, password,
				calendar, userLevel1);
		User user2 = new User(firstname + "2", lastname + "2", address, email,
				password + "2", calendar, userLevel2);
		restaurant.addManager(user1);
		assertTrue(restaurant.canAddDish(user1) == (true));
		assertTrue(!restaurant.canAddDish(user2) == (true));
	}

	@Test
	public void getDishTest() {
		Dish dish = new Dish("dish", "dish description", 40);
		dish.setId(1);
		List<Dish> dishlist = new LinkedList<Dish>();
		dishlist.add(dish);
		MenuCategory menuCategory = new MenuCategory(dishlist, "menu category");
		List<MenuCategory> menulist = new LinkedList<MenuCategory>();
		menulist.add(menuCategory);
		restaurant.setMenu(menulist);
		assertTrue(restaurant.getDish(1).equals(dish));
	}

	@Test
	public void addDishTest() {
		Dish dish = new Dish("dish", "dish description", 40);
		dish.setId(1);
		String category = "meny category";
		restaurant.addDish(dish, category);
		assertTrue(restaurant.getDish(1).equals(dish));
		assertTrue(restaurant.getDish(1).getMenuCategory()
				.equals(category));
	}
}
