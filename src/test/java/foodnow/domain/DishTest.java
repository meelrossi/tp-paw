package foodnow.domain;

import org.junit.Before;
import org.junit.Test;

import ar.edu.itba.it.paw.domain.Dish;

public class DishTest {

	Dish dish;

	@Before
	public void setup() {
		dish = new Dish("dish", "dish description", 40);
	}

	@Test(expected = IllegalStateException.class)
	public void tooLongDishNameTest() {
		dish.setName("too long dish nameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
	}

	@Test(expected = IllegalStateException.class)
	public void tooLongDishDescriptionTest() {
		dish.setDescription("too long dish description, too long dish description, too long dish description, too long dish description, too long dish description, too long dish description, too long dish description, too long dish description, too long dish description, too long dish description, too long dish description, too long dish description, ");
	}
	
	@Test(expected = IllegalStateException.class)
	public void invalidDishPriceTest() {
		dish.setPrice(-40);
	}
}
