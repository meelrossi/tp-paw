package foodnow.domain;

import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ar.edu.itba.it.paw.domain.Address;
import ar.edu.itba.it.paw.domain.DeliveryTime;
import ar.edu.itba.it.paw.domain.Dish;
import ar.edu.itba.it.paw.domain.FNOrder;
import ar.edu.itba.it.paw.domain.Neighborhood;
import ar.edu.itba.it.paw.domain.OrderItem;
import ar.edu.itba.it.paw.domain.Restaurant;
import ar.edu.itba.it.paw.domain.User;

public class FNOrderTest {
	
	public FNOrder order;
	public User user;
	public Restaurant restaurant;
	
	@Before
	public void setup() {
		Neighborhood neighborhood = new Neighborhood("neighborhood");
		Address address = new Address("street", 123, neighborhood);
		user = new User("firstname", "lastname", address, "email@paw.com", "password", Calendar.getInstance(), 2);
		Neighborhood restneighborhood = new Neighborhood("neighborhood2");
		Address restaddress = new Address("street2", 456, restneighborhood);
		DeliveryTime deliveryTime = new DeliveryTime("18:00", "23:59");
		restaurant = new Restaurant("name", "This is a restaurant", 30, restaddress, deliveryTime);
	}
	
	@Test
	public void validOrder() {
		Dish dish = new Dish("dish", "dish description", 40);
		dish.setId(1);
		OrderItem orderItem = new OrderItem(dish, 5);
		List<OrderItem> orderlist = new LinkedList<OrderItem>();
		orderlist.add(orderItem);
		order = new FNOrder(user, orderlist, restaurant, new Date());
	}
	
	@Test(expected = IllegalStateException.class)
	public void invalidUserTest() {
		Dish dish = new Dish("dish", "dish description", 40);
		dish.setId(1);
		OrderItem orderItem = new OrderItem(dish, 5);
		List<OrderItem> orderlist = new LinkedList<OrderItem>();
		orderlist.add(orderItem);
		order = new FNOrder(null, orderlist, restaurant, new Date());
	}
	
	@Test(expected = IllegalStateException.class)
	public void invalidDishListTest() {
		Dish dish = new Dish("dish", "dish description", 40);
		dish.setId(1);
		OrderItem orderItem = new OrderItem(dish, 5);
		List<OrderItem> orderlist = new LinkedList<OrderItem>();
		orderlist.add(orderItem);
		order = new FNOrder(user, null, restaurant, new Date());
	}
	
	@Test(expected = IllegalStateException.class)
	public void invalidRestaurantTest() {
		Dish dish = new Dish("dish", "dish description", 40);
		dish.setId(1);
		OrderItem orderItem = new OrderItem(dish, 5);
		List<OrderItem> orderlist = new LinkedList<OrderItem>();
		orderlist.add(orderItem);
		order = new FNOrder(user, orderlist, null, new Date());
	}
	
	@Test(expected = IllegalStateException.class)
	public void invalidDateTest() {
		Dish dish = new Dish("dish", "dish description", 40);
		dish.setId(1);
		OrderItem orderItem = new OrderItem(dish, 5);
		List<OrderItem> orderlist = new LinkedList<OrderItem>();
		orderlist.add(orderItem);
		order = new FNOrder(user, orderlist, restaurant, null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void emptyOrderTest() {
		List<OrderItem> orderlist = new LinkedList<OrderItem>();
		order = new FNOrder(user, orderlist, restaurant, new Date());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void invalidTotalTest() {
		Dish dish = new Dish("dish", "dish description", 40);
		dish.setId(1);
		OrderItem orderItem = new OrderItem(dish, 5);
		List<OrderItem> orderlist = new LinkedList<OrderItem>();
		orderlist.add(orderItem);
		order = new FNOrder(user, orderlist, -10, restaurant, new Date());
	}
	
	@Test
	public void deliveredTest() {
		Dish dish = new Dish("dish", "dish description", 40);
		dish.setId(1);
		OrderItem orderItem = new OrderItem(dish, 5);
		List<OrderItem> orderlist = new LinkedList<OrderItem>();
		orderlist.add(orderItem);
		order = new FNOrder(user, orderlist, restaurant, new Date());
		assertTrue(!order.getDelivered());
		order.setDelivered(true);
		assertTrue(order.getDelivered());
	}
}
