package foodnow.schema;

import java.sql.SQLException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.dialect.Dialect;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean;

public class PrintSchema {
	public static void main(String[] args) throws SQLException {
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.ERROR);

		ApplicationContext ctx = new
				ClassPathXmlApplicationContext("standalone.xml");

		AnnotationSessionFactoryBean factory =
				(AnnotationSessionFactoryBean) ctx.getBean("&sessionFactory");

		SessionFactory sessionFactory =  factory.getObject();
		Session session = sessionFactory.openSession();
		Dialect dialect =
				Dialect.getDialect(factory.getConfiguration().getProperties());
		String[] sql =
				factory.getConfiguration().generateSchemaCreationScript(dialect);
		session.close();
		for (String line : sql) {
			System.out.println(line+";");
		}

	}
}
