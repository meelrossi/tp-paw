CREATE TABLE action (
    name character(30) NOT NULL,
    user_level integer NOT NULL,
    description character(50) NOT NULL,
    page character(50) NOT NULL
);

CREATE TABLE address (
    id integer NOT NULL,
    street character varying(50) NOT NULL,
    restaurant_id integer,
    fn_user_id integer,
    street_number integer
);

CREATE TABLE dish (
    id integer NOT NULL,
    description text NOT NULL,
    price real NOT NULL,
    name character varying(30) NOT NULL,
    restaurant_id integer NOT NULL,
    menu_category character varying(30) NOT NULL
);

CREATE TABLE fn_order (
    id integer NOT NULL,
    fn_user_id integer NOT NULL,
    address_id integer NOT NULL,
    total real NOT NULL,
    restaurant_id integer NOT NULL,
    fn_order_date timestamp without time zone NOT NULL
);

CREATE TABLE fn_order_dish (
    fn_order_id integer NOT NULL,
    dish_id integer NOT NULL,
    dish_amount integer NOT NULL
);

CREATE TABLE fn_user (
    id integer NOT NULL,
    name character varying(25) NOT NULL,
    lastname character varying(25) NOT NULL,
    email character varying(50) NOT NULL,
    birthdate date NOT NULL,
    password character varying(20) NOT NULL,
    fn_user_level integer NOT NULL
);

CREATE TABLE manager_restaurant (
    restaurant_id integer NOT NULL,
    user_id integer NOT NULL
);

CREATE TABLE rating (
    description text NOT NULL,
    fn_user_id integer NOT NULL,
    restaurant_id integer NOT NULL,
    rating_value real DEFAULT 0 NOT NULL
);

CREATE TABLE restaurant (
    id integer NOT NULL,
    minamount real NOT NULL,
    description text NOT NULL,
    name character varying(30) NOT NULL,
    delivery_cost real,
    available_time text
);

ALTER TABLE ONLY action
    ADD CONSTRAINT action_pkey PRIMARY KEY (name, user_level);

ALTER TABLE ONLY address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);

ALTER TABLE ONLY dish
    ADD CONSTRAINT dish_pkey PRIMARY KEY (id);

ALTER TABLE ONLY fn_order_dish
    ADD CONSTRAINT fn_order_dish_pkey PRIMARY KEY (fn_order_id, dish_id);

ALTER TABLE ONLY fn_order
    ADD CONSTRAINT fn_order_pkey PRIMARY KEY (id);

ALTER TABLE ONLY fn_user
    ADD CONSTRAINT fn_user_pkey PRIMARY KEY (id);

ALTER TABLE ONLY manager_restaurant
    ADD CONSTRAINT manager_restaurant_pkey PRIMARY KEY (restaurant_id, user_id);

ALTER TABLE ONLY rating
    ADD CONSTRAINT rating_pkey PRIMARY KEY (fn_user_id, restaurant_id);

ALTER TABLE ONLY restaurant
    ADD CONSTRAINT restaurant_pkey PRIMARY KEY (id);

ALTER TABLE ONLY address
    ADD CONSTRAINT address_fn_user_id_fkey FOREIGN KEY (fn_user_id) REFERENCES fn_user(id);

ALTER TABLE ONLY address
    ADD CONSTRAINT address_restaurant_id_fkey FOREIGN KEY (restaurant_id) REFERENCES restaurant(id) ON DELETE CASCADE;

ALTER TABLE ONLY dish
    ADD CONSTRAINT dish_restaurant_id_fkey FOREIGN KEY (restaurant_id) REFERENCES restaurant(id) ON DELETE CASCADE;

ALTER TABLE ONLY fn_order
    ADD CONSTRAINT fn_order_address_id_fkey FOREIGN KEY (address_id) REFERENCES address(id);

ALTER TABLE ONLY fn_order_dish
    ADD CONSTRAINT fn_order_dish_dish_id_fkey FOREIGN KEY (dish_id) REFERENCES dish(id);

ALTER TABLE ONLY fn_order_dish
    ADD CONSTRAINT fn_order_dish_fn_order_id_fkey FOREIGN KEY (fn_order_id) REFERENCES fn_order(id);

ALTER TABLE ONLY fn_order
    ADD CONSTRAINT fn_order_fn_user_id_fkey FOREIGN KEY (fn_user_id) REFERENCES fn_user(id);

ALTER TABLE ONLY fn_order
    ADD CONSTRAINT fn_order_restaurant_id_fkey FOREIGN KEY (restaurant_id) REFERENCES restaurant(id);

ALTER TABLE ONLY manager_restaurant
    ADD CONSTRAINT manager_restaurant_restaurant_id_fkey FOREIGN KEY (restaurant_id) REFERENCES restaurant(id) ON DELETE CASCADE;

ALTER TABLE ONLY manager_restaurant
    ADD CONSTRAINT manager_restaurant_user_id_fkey FOREIGN KEY (user_id) REFERENCES fn_user(id);

ALTER TABLE ONLY rating
    ADD CONSTRAINT rating_fn_user_id_fkey FOREIGN KEY (fn_user_id) REFERENCES fn_user(id);

ALTER TABLE ONLY rating
    ADD CONSTRAINT rating_restaurant_id_fkey FOREIGN KEY (restaurant_id) REFERENCES restaurant(id) ON DELETE CASCADE;