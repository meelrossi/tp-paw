INSERT INTO action (name, user_level, description, page) VALUES 
('addRestaurant', 0, 'Agregar Restoran', 'header'),
('addManager', 0, 'Agregar gerente', 'header'),
('removeRestaurant', 0, 'Eliminar Restoran', 'restaurantList'),
('editRestaurant', 0, 'Editar Restoran', 'restaurantList'),
('rateRestaurant', 2, 'Calificar Restoran', 'restaurant');

INSERT INTO fn_user (id, name, lastname, email, birthdate, password, fn_user_level) VALUES
(1,	'Melisa',	'Rossi',	'meliarossi@hotmail.com', '1994-03-03',	'melisarossi',	0),
(2,	'Agustin', 'Mounier',	'agusmounier@gmail.com', '1993-09-28',	'agustinmounier',	0),
(3,	'Franco',	'Zannini',	'francozannini@gmail.com', '1994-12-01',	'francozannini',	0),
(4,	'Juan',	'Gomez',	'juangomez@hotmail.com', '1984-12-02',	'password',	1),
(5,	'Julieta',	'Perez',	'julietaperez@hotmail.com', '1970-03-20',	'password',	1),
(6,	'Manuela',	'Vazquez',	'manuelavazquez@hotmail.com', '1970-11-15',	'password',	1),
(7,	'Camila',	'Galvan',	'camilagalvan@gmail.com', '1970-05-06',	'password',	2),
(8,	'Bruno',	'Slongo',	'brunoslongo@gmail.com', '1993-07-02',	'password',	2),
(9,	'Sebastian',	'Pina',	'sebastianpina@gmail.com', '1993-10-07',	'password',	2),
(10,	'hola',	'hola',	'hola2@gmail.com', '2015-01-09',	'hola',	1),
(11,	'Gerente',	'Test',	'gerentetest@gmail.com', '2015-01-29',	'gerente',	1),
(12,	'Gerente',	'Test',	'gerentetest2@gmail.com', '2015-01-10',	'gerente',	1),
(13,	'Juan Carlos',	'Rodriguez',	'juanrodriguez@gmail.com', '1990-01-24',	'juanrodriguez',	2),
(14,	'Leila',	'Rossi',	'leilaarossi@gmail.com', '1991-01-30',	'password',	2),
(15,	'Juan Carlos',	'Rodriguez',	'juanrodriguez2@gmail.com', '1990-01-24',	'juanrodriguez',	2),
(16,	'Sebastian',	'Rossi',	'sebirossi@hotmail.com', '1999-01-18',	'password',	2),
(17,	'Pedro',	'Alfonso',	'pedroalfonso@gmail.com', '1986-01-09',	'password',	1),
(18,	'Pedro',	'Perez',	'pedroperez@gmail.com', '1990-01-22',	'pedroperez',	1);


INSERT INTO restaurant (id, minamount, description, name, delivery_cost, available_time) VALUES (1	,200,'American Grill. La mejor carne.','Kansas',40,'Lunes a viernes de 8 a 18hs');
INSERT INTO restaurant (id, minamount, description, name, delivery_cost, available_time) VALUES (7	,80,'Platos tradicionales de oriente medio','Sarkis',20,'Lunes a Viernes de 8 a 24 hrs');
INSERT INTO restaurant (id, minamount, description, name, delivery_cost, available_time) VALUES (8	,100,'Restaurante argentino',	'La Cabrera',40,'Lunes a Viernes de 8 a 24 hrs');
INSERT INTO restaurant (id, minamount, description, name, delivery_cost, available_time) VALUES (9	,100,'Restaurante especializado en platos a la barbacoa.','El Gran Mosquito',50,'Lunes a Viernes de 8 a 24 hrs');
INSERT INTO restaurant (id, minamount, description, name, delivery_cost, available_time) VALUES (10	,100,'Restaurante argentino',	'La Bistecca',50,'Lunes a Viernes de 8 a 24 hrs');
INSERT INTO restaurant (id, minamount, description, name, delivery_cost, available_time) VALUES (11	,100,'Restaurant de comida italiana.','La Parolaccia',50,'Lunes a Viernes de 8 a 24 hrs');

INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (1	,'Roosevelt', NULL ,	1,	5471);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (2	,'Monroe', NULL ,	2,	2145);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (3	,'Olazaba', NULL ,	3,	5432);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (4	,'Triunvirato', NULL ,	4,	3241);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (5	,'Cabildo', NULL ,	5,	6543);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (6	,'Av. de Mayo', NULL ,	6,	655);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (7	,'Avellaneda', NULL ,	7,	1234);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (8	,'Rivera', NULL ,	8,	6543);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (9	,'Baunes', NULL ,	9,	6798);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (10	,'Calle Serrano', 1 , NULL , 4567);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (11	,'Rivera', NULL ,	17,	1234);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (12	,'quesada', NULL ,	18,	1234);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (19	,'Thames', 7 ,NULL ,1101);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (20	,'Jos?Antonio Cabrera', 8, NULL ,5099);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (21	,'Tte. Gral. Juan Domingo Peron', 9 , NULL ,4499);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (22	,'Av Alicia Moreau de Justo ',	10, NULL ,	1890);
INSERT INTO ADDRESS (id, street, restaurant_id, fn_user_id, street_number) VALUES (23	,'Av. Cervinio', 11, NULL ,	3590);

INSERT INTO DISH (id, description, price, name, restaurant_id, menu_category) VALUES (1,'Sabrosas costillas de cerdo asadas en suave salsa barbacoa con papas asadas.',200,'Ribs',1,'Menu Principal');
INSERT INTO DISH (id, description, price, name, restaurant_id, menu_category) VALUES (3,'500 ml',20,'Coca Cola Light',1,'Bebidas');
INSERT INTO DISH (id, description, price, name, restaurant_id, menu_category) VALUES (4,'',30,'Coca cola',11,'Bebidas');
INSERT INTO DISH (id, description, price, name, restaurant_id, menu_category) VALUES (5,'Pizza de muza',100,'Pizza',11,'Plato Principal');
INSERT INTO DISH (id, description, price, name, restaurant_id, menu_category) VALUES (6,'',20,'Pepsi',10,'Bebidas');
INSERT INTO DISH (id, description, price, name, restaurant_id, menu_category) VALUES (7,'',150,'Bife de chorizo',10,'Platos Principales');
INSERT INTO DISH (id, description, price, name, restaurant_id, menu_category) VALUES (8,'',20,'7up',8,'Bebidas');
INSERT INTO DISH (id, description, price, name, restaurant_id, menu_category) VALUES (9,'',150,'Milanesa napolitana',8,'Platos Principales');
INSERT INTO DISH (id, description, price, name, restaurant_id, menu_category) VALUES (2,'500 ml',20,'Coca Cola',1,'Bebidas');

INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (1	,3	,3	,200	,1	,'2015-09-20 20:17:39.713');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (2	,3	,3	,200	,1	,'2015-09-20 20:17:39.713');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (3	,3	,3	,200	,1	,'2015-09-20 20:27:27.026');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (4	,3	,3	,200	,1	,'2015-09-20 20:30:47.095');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (5	,3	,3	,200	,1	,'2015-09-20 20:31:49.507');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (6	,3	,3	,200	,1	,'2015-09-20 20:40:11.655');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (7	,3	,3	,200	,1	,'2015-09-20 20:41:22.464');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (8	,3	,3	,200	,1	,'2015-09-20 20:43:46.478');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (9	,3	,3	,200	,1	,'2015-09-20 20:47:49.699');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (10,	3,	3,	200,	1,'	2015-09-20 20:49:42.571');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (11,	3,	3,	200,	1,'	2015-09-20 20:53:37.965');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (12,	3,	3,	200,	1,'	2015-09-20 20:56:53.881');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (13,	1,	1,	200,	1,'	2015-09-20 22:24:33.117');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (14,	3,	3,	240,	1,'	2015-09-20 23:39:34.437');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (15,	3,	3,	240,	1,'	2015-09-20 23:40:58.529');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (16,	3,	3,	240,	1,'	2015-09-20 23:41:10.817');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (17,	3,	3,	240,	1,'	2015-09-20 23:41:22.141');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (18,	3,	3,	240,	1,'	2015-09-20 23:44:08.038');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (19,	3,	3,	240,	1,'	2015-09-20 23:49:20.323');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (20,	3,	3,	240,	1,'	2015-09-20 23:50:07.099');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (21,	3,	3,	320,	1,'	2015-09-21 00:11:25.895');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (22,	3,	3,	480,	1,'	2015-09-21 00:20:52.127');
INSERT INTO fn_order (id, fn_user_id, address_id, total, restaurant_id, fn_order_date) VALUES (23,	7,	7,	280,	1,'	2015-09-21 00:43:44.082');

INSERT INTO fn_order_dish (fn_order_id, dish_id, dish_amount) VALUES
(5,	1,	1),
(6,	1,	1),
(7,	1,	1),
(8,	1,	1),
(9,	1,	1),
(10,	1,	1),
(11,	1,	1),
(12,	1,	1),
(13,	1,	1),
(16,	3,	1),
(17,	2,	3),
(17,	1,	1),
(17,	3,	1),
(18,	1,	2),
(19,	1,	1),
(19,	3,	3),
(20,	1,	1),
(20,	3,	3),
(21,	3,	4),
(21,	1,	1),
(22,	1,	2),
(22,	3,	2),
(23,	1,	1),
(23,	2,	2);

INSERT INTO manager_restaurant (restaurant_id, user_id) VALUES
(1,	2),
(1,	11),
(1,	18);

INSERT INTO rating (description, fn_user_id, restaurant_id, rating_value) VALUES ('Muy rico todo!',	1,	1,	3);
INSERT INTO rating (description, fn_user_id, restaurant_id, rating_value) VALUES ('Riquisimo aunque muy caro',	7,	1,	4);











INSERT INTO action (description, name, page, user_level) VALUES 
('Agregar Restoran','addRestaurant', 'header', 0),
('Agregar gerente','addManager','header', 0),
('Eliminar Restoran','removeRestaurant','restaurantList', 0),
('Editar Restoran','editRestaurant', 'restaurantList',0),
('Calificar Restoran','rateRestaurant', 'restaurant', 2);
